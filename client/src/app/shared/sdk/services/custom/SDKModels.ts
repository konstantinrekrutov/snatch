/* tslint:disable */
import { Injectable } from '@angular/core';
import { Email } from '../../models/Email';
import { MyUser } from '../../models/MyUser';
import { MyAccessToken } from '../../models/MyAccessToken';
import { Creditionals } from '../../models/Creditionals';
import { Tarif } from '../../models/Tarif';
import { LimitCount } from '../../models/LimitCount';
import { CopyTask } from '../../models/CopyTask';
import { Contacts } from '../../models/Contacts';
import { TelegramUser } from '../../models/TelegramUser';
import { Notification } from '../../models/Notification';
import { LastDialog } from '../../models/LastDialog';
import { Wallet } from '../../models/Wallet';
import { Order } from '../../models/Order';
import { TestPeriod } from '../../models/TestPeriod';
import { UserTarif } from '../../models/UserTarif';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Email: Email,
    MyUser: MyUser,
    MyAccessToken: MyAccessToken,
    Creditionals: Creditionals,
    Tarif: Tarif,
    LimitCount: LimitCount,
    CopyTask: CopyTask,
    Contacts: Contacts,
    TelegramUser: TelegramUser,
    Notification: Notification,
    LastDialog: LastDialog,
    Wallet: Wallet,
    Order: Order,
    TestPeriod: TestPeriod,
    UserTarif: UserTarif,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
