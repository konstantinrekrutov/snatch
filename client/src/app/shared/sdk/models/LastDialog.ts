/* tslint:disable */
import {
  Creditionals
} from '../index';

declare var Object: any;
export interface LastDialogInterface {
  "date": number;
  "id"?: any;
  "creditionalsId"?: any;
  creditionals?: Creditionals;
}

export class LastDialog implements LastDialogInterface {
  "date": number;
  "id": any;
  "creditionalsId": any;
  creditionals: Creditionals;
  constructor(data?: LastDialogInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `LastDialog`.
   */
  public static getModelName() {
    return "LastDialog";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of LastDialog for dynamic purposes.
  **/
  public static factory(data: LastDialogInterface): LastDialog{
    return new LastDialog(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'LastDialog',
      plural: 'LastDialogs',
      path: 'LastDialogs',
      idName: 'id',
      properties: {
        "date": {
          name: 'date',
          type: 'number',
          default: 0
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "creditionalsId": {
          name: 'creditionalsId',
          type: 'any'
        },
      },
      relations: {
        creditionals: {
          name: 'creditionals',
          type: 'Creditionals',
          model: 'Creditionals',
          relationType: 'belongsTo',
                  keyFrom: 'creditionalsId',
          keyTo: 'id'
        },
      }
    }
  }
}
