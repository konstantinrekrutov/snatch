/* tslint:disable */

declare var Object: any;
export interface TelegramUserInterface {
  "id": number;
  "title"?: string;
  "first_name"?: string;
  "flags"?: number;
  "bot"?: boolean;
  "last_name"?: string;
  "phone"?: string;
  "status"?: any;
  contacts?: TelegramUser[];
}

export class TelegramUser implements TelegramUserInterface {
  "id": number;
  "title": string;
  "first_name": string;
  "flags": number;
  "bot": boolean;
  "last_name": string;
  "phone": string;
  "status": any;
  contacts: TelegramUser[];
  constructor(data?: TelegramUserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `TelegramUser`.
   */
  public static getModelName() {
    return "TelegramUser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of TelegramUser for dynamic purposes.
  **/
  public static factory(data: TelegramUserInterface): TelegramUser{
    return new TelegramUser(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'TelegramUser',
      plural: 'TelegramUsers',
      path: 'TelegramUsers',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "title": {
          name: 'title',
          type: 'string'
        },
        "first_name": {
          name: 'first_name',
          type: 'string'
        },
        "flags": {
          name: 'flags',
          type: 'number'
        },
        "bot": {
          name: 'bot',
          type: 'boolean',
          default: false
        },
        "last_name": {
          name: 'last_name',
          type: 'string'
        },
        "phone": {
          name: 'phone',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'any'
        },
      },
      relations: {
        contacts: {
          name: 'contacts',
          type: 'TelegramUser[]',
          model: 'TelegramUser',
          relationType: 'hasMany',
          modelThrough: 'Telegram_user_contacts',
          keyThrough: 'telegramUserId',
          keyFrom: 'id',
          keyTo: 'contactId'
        },
      }
    }
  }
}
