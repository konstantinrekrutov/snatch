/* tslint:disable */
import {
  Wallet,
  Tarif,
  MyUser
} from '../index';

declare var Object: any;
export interface OrderInterface {
  "price": number;
  "tarifId": any;
  "myUserId": any;
  "walletId": any;
  "date": Date;
  "days": number;
  "id"?: any;
  wallet?: Wallet;
  tarif?: Tarif;
  myUser?: MyUser;
}

export class Order implements OrderInterface {
  "price": number;
  "tarifId": any;
  "myUserId": any;
  "walletId": any;
  "date": Date;
  "days": number;
  "id": any;
  wallet: Wallet;
  tarif: Tarif;
  myUser: MyUser;
  constructor(data?: OrderInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Order`.
   */
  public static getModelName() {
    return "Order";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Order for dynamic purposes.
  **/
  public static factory(data: OrderInterface): Order{
    return new Order(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Order',
      plural: 'Orders',
      path: 'Orders',
      idName: 'id',
      properties: {
        "price": {
          name: 'price',
          type: 'number',
          default: 0
        },
        "tarifId": {
          name: 'tarifId',
          type: 'any'
        },
        "myUserId": {
          name: 'myUserId',
          type: 'any'
        },
        "walletId": {
          name: 'walletId',
          type: 'any'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "days": {
          name: 'days',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        wallet: {
          name: 'wallet',
          type: 'Wallet',
          model: 'Wallet',
          relationType: 'belongsTo',
                  keyFrom: 'walletId',
          keyTo: 'id'
        },
        tarif: {
          name: 'tarif',
          type: 'Tarif',
          model: 'Tarif',
          relationType: 'belongsTo',
                  keyFrom: 'tarifId',
          keyTo: 'id'
        },
        myUser: {
          name: 'myUser',
          type: 'MyUser',
          model: 'MyUser',
          relationType: 'belongsTo',
                  keyFrom: 'myUserId',
          keyTo: 'id'
        },
      }
    }
  }
}
