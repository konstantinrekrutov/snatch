/* tslint:disable */
import {
  MyUser
} from '../index';

declare var Object: any;
export interface UserTarifInterface {
  "myUserId": any;
  "canSendRawTo": Date;
  "canUseProxyTo": Date;
  "taskCnt": number;
  "id"?: any;
  "tarifId"?: any;
  myUser?: MyUser;
}

export class UserTarif implements UserTarifInterface {
  "myUserId": any;
  "canSendRawTo": Date;
  "canUseProxyTo": Date;
  "taskCnt": number;
  "id": any;
  "tarifId": any;
  myUser: MyUser;
  constructor(data?: UserTarifInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `UserTarif`.
   */
  public static getModelName() {
    return "UserTarif";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of UserTarif for dynamic purposes.
  **/
  public static factory(data: UserTarifInterface): UserTarif{
    return new UserTarif(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'UserTarif',
      plural: 'UserTarifs',
      path: 'UserTarifs',
      idName: 'id',
      properties: {
        "myUserId": {
          name: 'myUserId',
          type: 'any'
        },
        "canSendRawTo": {
          name: 'canSendRawTo',
          type: 'Date'
        },
        "canUseProxyTo": {
          name: 'canUseProxyTo',
          type: 'Date',
          default: new Date(0)
        },
        "taskCnt": {
          name: 'taskCnt',
          type: 'number',
          default: 4
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "tarifId": {
          name: 'tarifId',
          type: 'any'
        },
      },
      relations: {
        myUser: {
          name: 'myUser',
          type: 'MyUser',
          model: 'MyUser',
          relationType: 'belongsTo',
                  keyFrom: 'myUserId',
          keyTo: 'id'
        },
      }
    }
  }
}
