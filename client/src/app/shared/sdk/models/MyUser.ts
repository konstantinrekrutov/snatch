/* tslint:disable */
import {
  MyAccessToken,
  Creditionals,
  Tarif,
  CopyTask,
  Notification,
  Wallet,
  UserTarif
} from '../index';

declare var Object: any;
export interface MyUserInterface {
  "realm"?: string;
  "username"?: string;
  "email": string;
  "emailVerified"?: boolean;
  "id"?: any;
  "tarifId"?: any;
  "password"?: string;
  accessTokens?: MyAccessToken[];
  creditionals?: Creditionals[];
  tarifs?: Tarif;
  copyTasks?: CopyTask[];
  notifications?: Notification[];
  wallets?: Wallet[];
  userTarifs?: UserTarif;
}

export class MyUser implements MyUserInterface {
  "realm": string;
  "username": string;
  "email": string;
  "emailVerified": boolean;
  "id": any;
  "tarifId": any;
  "password": string;
  accessTokens: MyAccessToken[];
  creditionals: Creditionals[];
  tarifs: Tarif;
  copyTasks: CopyTask[];
  notifications: Notification[];
  wallets: Wallet[];
  userTarifs: UserTarif;
  constructor(data?: MyUserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `MyUser`.
   */
  public static getModelName() {
    return "MyUser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of MyUser for dynamic purposes.
  **/
  public static factory(data: MyUserInterface): MyUser{
    return new MyUser(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'MyUser',
      plural: 'MyUsers',
      path: 'MyUsers',
      idName: 'id',
      properties: {
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "tarifId": {
          name: 'tarifId',
          type: 'any'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'MyAccessToken[]',
          model: 'MyAccessToken',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
        creditionals: {
          name: 'creditionals',
          type: 'Creditionals[]',
          model: 'Creditionals',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'myUserId'
        },
        tarifs: {
          name: 'tarifs',
          type: 'Tarif',
          model: 'Tarif',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'myUserId'
        },
        copyTasks: {
          name: 'copyTasks',
          type: 'CopyTask[]',
          model: 'CopyTask',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'myUserId'
        },
        notifications: {
          name: 'notifications',
          type: 'Notification[]',
          model: 'Notification',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'myUserId'
        },
        wallets: {
          name: 'wallets',
          type: 'Wallet[]',
          model: 'Wallet',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'myUserId'
        },
        userTarifs: {
          name: 'userTarifs',
          type: 'UserTarif',
          model: 'UserTarif',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'myUserId'
        },
      }
    }
  }
}
