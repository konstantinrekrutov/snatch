/* tslint:disable */
import {
  TelegramUser
} from '../index';

declare var Object: any;
export interface TestPeriodInterface {
  "dateStart": Date;
  "dateEnd": Date;
  "telegramUserId": number;
  "id"?: any;
  telegramUser?: TelegramUser;
}

export class TestPeriod implements TestPeriodInterface {
  "dateStart": Date;
  "dateEnd": Date;
  "telegramUserId": number;
  "id": any;
  telegramUser: TelegramUser;
  constructor(data?: TestPeriodInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `TestPeriod`.
   */
  public static getModelName() {
    return "TestPeriod";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of TestPeriod for dynamic purposes.
  **/
  public static factory(data: TestPeriodInterface): TestPeriod{
    return new TestPeriod(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'TestPeriod',
      plural: 'TestPeriods',
      path: 'TestPeriods',
      idName: 'id',
      properties: {
        "dateStart": {
          name: 'dateStart',
          type: 'Date'
        },
        "dateEnd": {
          name: 'dateEnd',
          type: 'Date'
        },
        "telegramUserId": {
          name: 'telegramUserId',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        telegramUser: {
          name: 'telegramUser',
          type: 'TelegramUser',
          model: 'TelegramUser',
          relationType: 'belongsTo',
                  keyFrom: 'telegramUserId',
          keyTo: 'id'
        },
      }
    }
  }
}
