/* tslint:disable */
import {
  MyUser
} from '../index';

declare var Object: any;
export interface NotificationInterface {
  "title"?: string;
  "description"?: string;
  "priority"?: number;
  "viewed"?: boolean;
  "id"?: any;
  "myUserId"?: any;
  myUser?: MyUser;
}

export class Notification implements NotificationInterface {
  "title": string;
  "description": string;
  "priority": number;
  "viewed": boolean;
  "id": any;
  "myUserId": any;
  myUser: MyUser;
  constructor(data?: NotificationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Notification`.
   */
  public static getModelName() {
    return "Notification";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Notification for dynamic purposes.
  **/
  public static factory(data: NotificationInterface): Notification{
    return new Notification(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Notification',
      plural: 'Notifications',
      path: 'Notifications',
      idName: 'id',
      properties: {
        "title": {
          name: 'title',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "priority": {
          name: 'priority',
          type: 'number',
          default: 0
        },
        "viewed": {
          name: 'viewed',
          type: 'boolean',
          default: false
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "myUserId": {
          name: 'myUserId',
          type: 'any'
        },
      },
      relations: {
        myUser: {
          name: 'myUser',
          type: 'MyUser',
          model: 'MyUser',
          relationType: 'belongsTo',
                  keyFrom: 'myUserId',
          keyTo: 'id'
        },
      }
    }
  }
}
