/* tslint:disable */
import {
  Tarif
} from '../index';

declare var Object: any;
export interface LimitCountInterface {
  "Creditionals": number;
  "CopyTask": number;
  "id"?: any;
  "tarifId"?: any;
  tarif?: Tarif;
}

export class LimitCount implements LimitCountInterface {
  "Creditionals": number;
  "CopyTask": number;
  "id": any;
  "tarifId": any;
  tarif: Tarif;
  constructor(data?: LimitCountInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `LimitCount`.
   */
  public static getModelName() {
    return "LimitCount";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of LimitCount for dynamic purposes.
  **/
  public static factory(data: LimitCountInterface): LimitCount{
    return new LimitCount(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'LimitCount',
      plural: 'LimitCounts',
      path: 'LimitCounts',
      idName: 'id',
      properties: {
        "Creditionals": {
          name: 'Creditionals',
          type: 'number'
        },
        "CopyTask": {
          name: 'CopyTask',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "tarifId": {
          name: 'tarifId',
          type: 'any'
        },
      },
      relations: {
        tarif: {
          name: 'tarif',
          type: 'Tarif',
          model: 'Tarif',
          relationType: 'belongsTo',
                  keyFrom: 'tarifId',
          keyTo: 'id'
        },
      }
    }
  }
}
