/* tslint:disable */
import {
  MyUser,
  CopyTask,
  TelegramUser,
  LastDialog
} from '../index';

declare var Object: any;
export interface CreditionalsInterface {
  "api_id": number;
  "status": number;
  "api_hash": string;
  "phone": string;
  "code"?: string;
  "phone_code_hash"?: string;
  "id"?: any;
  "myUserId"?: any;
  "created"?: Date;
  "modified"?: Date;
  "telegramUserId"?: number;
  storages?: any;
  myUser?: MyUser;
  copyTasks?: CopyTask[];
  telegramUser?: TelegramUser;
  lastDialog?: LastDialog;
}

export class Creditionals implements CreditionalsInterface {
  "api_id": number;
  "status": number;
  "api_hash": string;
  "phone": string;
  "code": string;
  "phone_code_hash": string;
  "id": any;
  "myUserId": any;
  "created": Date;
  "modified": Date;
  "telegramUserId": number;
  storages: any;
  myUser: MyUser;
  copyTasks: CopyTask[];
  telegramUser: TelegramUser;
  lastDialog: LastDialog;
  constructor(data?: CreditionalsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Creditionals`.
   */
  public static getModelName() {
    return "Creditionals";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Creditionals for dynamic purposes.
  **/
  public static factory(data: CreditionalsInterface): Creditionals{
    return new Creditionals(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Creditionals',
      plural: 'Creditionals',
      path: 'Creditionals',
      idName: 'id',
      properties: {
        "api_id": {
          name: 'api_id',
          type: 'number'
        },
        "status": {
          name: 'status',
          type: 'number',
          default: 1
        },
        "api_hash": {
          name: 'api_hash',
          type: 'string'
        },
        "phone": {
          name: 'phone',
          type: 'string'
        },
        "code": {
          name: 'code',
          type: 'string'
        },
        "phone_code_hash": {
          name: 'phone_code_hash',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "myUserId": {
          name: 'myUserId',
          type: 'any'
        },
        "created": {
          name: 'created',
          type: 'Date',
          default: new Date(0)
        },
        "modified": {
          name: 'modified',
          type: 'Date',
          default: new Date(0)
        },
        "telegramUserId": {
          name: 'telegramUserId',
          type: 'number'
        },
      },
      relations: {
        storages: {
          name: 'storages',
          type: 'any',
          model: '',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'creditionalsId'
        },
        myUser: {
          name: 'myUser',
          type: 'MyUser',
          model: 'MyUser',
          relationType: 'belongsTo',
                  keyFrom: 'myUserId',
          keyTo: 'id'
        },
        copyTasks: {
          name: 'copyTasks',
          type: 'CopyTask[]',
          model: 'CopyTask',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'creditionalsId'
        },
        telegramUser: {
          name: 'telegramUser',
          type: 'TelegramUser',
          model: 'TelegramUser',
          relationType: 'belongsTo',
                  keyFrom: 'telegramUserId',
          keyTo: 'id'
        },
        lastDialog: {
          name: 'lastDialog',
          type: 'LastDialog',
          model: 'LastDialog',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'creditionalsId'
        },
      }
    }
  }
}
