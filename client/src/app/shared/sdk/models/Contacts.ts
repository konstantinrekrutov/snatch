/* tslint:disable */
import {
  TelegramUser
} from '../index';

declare var Object: any;
export interface ContactsInterface {
  "user_id"?: number;
  telegramUser?: TelegramUser;
  telegramUsers?: TelegramUser[];
}

export class Contacts implements ContactsInterface {
  "user_id": number;
  telegramUser: TelegramUser;
  telegramUsers: TelegramUser[];
  constructor(data?: ContactsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Contacts`.
   */
  public static getModelName() {
    return "Contacts";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Contacts for dynamic purposes.
  **/
  public static factory(data: ContactsInterface): Contacts{
    return new Contacts(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Contacts',
      plural: 'Contacts',
      path: 'Contacts',
      idName: 'user_id',
      properties: {
        "user_id": {
          name: 'user_id',
          type: 'number'
        },
      },
      relations: {
        telegramUser: {
          name: 'telegramUser',
          type: 'TelegramUser',
          model: 'TelegramUser',
          relationType: 'belongsTo',
                  keyFrom: 'user_id',
          keyTo: 'id'
        },
        telegramUsers: {
          name: 'telegramUsers',
          type: 'TelegramUser[]',
          model: 'TelegramUser',
          relationType: 'hasMany',
          modelThrough: 'Telegram_user_contacts',
          keyThrough: 'telegramUserId',
          keyFrom: 'user_id',
          keyTo: 'contactId'
        },
      }
    }
  }
}
