/* tslint:disable */
import {
  MyUser,
  Tarif,
  Order
} from '../index';

declare var Object: any;
export interface WalletInterface {
  "tokenId": string;
  "lastBuyTransaction"?: number;
  "tarifId": any;
  "myUserId": any;
  "id"?: any;
  myUser?: MyUser;
  tarif?: Tarif;
  orders?: Order[];
}

export class Wallet implements WalletInterface {
  "tokenId": string;
  "lastBuyTransaction": number;
  "tarifId": any;
  "myUserId": any;
  "id": any;
  myUser: MyUser;
  tarif: Tarif;
  orders: Order[];
  constructor(data?: WalletInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Wallet`.
   */
  public static getModelName() {
    return "Wallet";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Wallet for dynamic purposes.
  **/
  public static factory(data: WalletInterface): Wallet{
    return new Wallet(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Wallet',
      plural: 'Wallets',
      path: 'Wallets',
      idName: 'id',
      properties: {
        "tokenId": {
          name: 'tokenId',
          type: 'string'
        },
        "lastBuyTransaction": {
          name: 'lastBuyTransaction',
          type: 'number'
        },
        "tarifId": {
          name: 'tarifId',
          type: 'any'
        },
        "myUserId": {
          name: 'myUserId',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        myUser: {
          name: 'myUser',
          type: 'MyUser',
          model: 'MyUser',
          relationType: 'belongsTo',
                  keyFrom: 'myUserId',
          keyTo: 'id'
        },
        tarif: {
          name: 'tarif',
          type: 'Tarif',
          model: 'Tarif',
          relationType: 'belongsTo',
                  keyFrom: 'tarifId',
          keyTo: 'id'
        },
        orders: {
          name: 'orders',
          type: 'Order[]',
          model: 'Order',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'walletId'
        },
      }
    }
  }
}
