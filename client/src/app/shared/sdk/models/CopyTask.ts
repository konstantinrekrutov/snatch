/* tslint:disable */
import {
  Creditionals,
  MyUser
} from '../index';

declare var Object: any;
export interface CopyTaskInterface {
  "from": number;
  "to": number;
  "last_message"?: number;
  "sendRaw"?: boolean;
  "started"?: boolean;
  "creditionalsProxyId"?: string;
  "id"?: any;
  "creditionalsId"?: any;
  "myUserId"?: any;
  "created"?: Date;
  "modified"?: Date;
  creditionals?: Creditionals;
  myUser?: MyUser;
}

export class CopyTask implements CopyTaskInterface {
  "from": number;
  "to": number;
  "last_message": number;
  "sendRaw": boolean;
  "started": boolean;
  "creditionalsProxyId": string;
  "id": any;
  "creditionalsId": any;
  "myUserId": any;
  "created": Date;
  "modified": Date;
  creditionals: Creditionals;
  myUser: MyUser;
  constructor(data?: CopyTaskInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `CopyTask`.
   */
  public static getModelName() {
    return "CopyTask";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of CopyTask for dynamic purposes.
  **/
  public static factory(data: CopyTaskInterface): CopyTask{
    return new CopyTask(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'CopyTask',
      plural: 'CopyTasks',
      path: 'CopyTasks',
      idName: 'id',
      properties: {
        "from": {
          name: 'from',
          type: 'number'
        },
        "to": {
          name: 'to',
          type: 'number'
        },
        "last_message": {
          name: 'last_message',
          type: 'number'
        },
        "sendRaw": {
          name: 'sendRaw',
          type: 'boolean',
          default: false
        },
        "started": {
          name: 'started',
          type: 'boolean',
          default: false
        },
        "creditionalsProxyId": {
          name: 'creditionalsProxyId',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "creditionalsId": {
          name: 'creditionalsId',
          type: 'any'
        },
        "myUserId": {
          name: 'myUserId',
          type: 'any'
        },
        "created": {
          name: 'created',
          type: 'Date',
          default: new Date(0)
        },
        "modified": {
          name: 'modified',
          type: 'Date',
          default: new Date(0)
        },
      },
      relations: {
        creditionals: {
          name: 'creditionals',
          type: 'Creditionals',
          model: 'Creditionals',
          relationType: 'belongsTo',
                  keyFrom: 'creditionalsId',
          keyTo: 'id'
        },
        myUser: {
          name: 'myUser',
          type: 'MyUser',
          model: 'MyUser',
          relationType: 'belongsTo',
                  keyFrom: 'myUserId',
          keyTo: 'id'
        },
      }
    }
  }
}
