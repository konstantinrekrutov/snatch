/* tslint:disable */
import {
  MyUser,
  LimitCount,
  Wallet,
  UserTarif
} from '../index';

declare var Object: any;
export interface TarifInterface {
  "default"?: boolean;
  "days": number;
  "title": string;
  "canSendRaw"?: boolean;
  "canUseProxy"?: boolean;
  "description"?: string;
  "price"?: number;
  "id"?: any;
  "myUserId"?: any;
  myUsers?: MyUser[];
  limitCounts?: LimitCount;
  wallets?: Wallet[];
  userTarifs?: UserTarif[];
}

export class Tarif implements TarifInterface {
  "default": boolean;
  "days": number;
  "title": string;
  "canSendRaw": boolean;
  "canUseProxy": boolean;
  "description": string;
  "price": number;
  "id": any;
  "myUserId": any;
  myUsers: MyUser[];
  limitCounts: LimitCount;
  wallets: Wallet[];
  userTarifs: UserTarif[];
  constructor(data?: TarifInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Tarif`.
   */
  public static getModelName() {
    return "Tarif";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Tarif for dynamic purposes.
  **/
  public static factory(data: TarifInterface): Tarif{
    return new Tarif(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Tarif',
      plural: 'Tarifs',
      path: 'Tarifs',
      idName: 'id',
      properties: {
        "default": {
          name: 'default',
          type: 'boolean',
          default: false
        },
        "days": {
          name: 'days',
          type: 'number',
          default: 30
        },
        "title": {
          name: 'title',
          type: 'string'
        },
        "canSendRaw": {
          name: 'canSendRaw',
          type: 'boolean',
          default: false
        },
        "canUseProxy": {
          name: 'canUseProxy',
          type: 'boolean',
          default: false
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "price": {
          name: 'price',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "myUserId": {
          name: 'myUserId',
          type: 'any'
        },
      },
      relations: {
        myUsers: {
          name: 'myUsers',
          type: 'MyUser[]',
          model: 'MyUser',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'tarifId'
        },
        limitCounts: {
          name: 'limitCounts',
          type: 'LimitCount',
          model: 'LimitCount',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'tarifId'
        },
        wallets: {
          name: 'wallets',
          type: 'Wallet[]',
          model: 'Wallet',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'tarifId'
        },
        userTarifs: {
          name: 'userTarifs',
          type: 'UserTarif[]',
          model: 'UserTarif',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'tarifId'
        },
      }
    }
  }
}
