import {Component, OnDestroy, OnInit, DoCheck} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {MyUserApi} from "../shared/sdk/services/custom";
import {AuthService} from "../services/auth/auth.service";




@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy, DoCheck {
    private sub: Subscription;
    currentUrl: string;
    menu: any;
    logStatus: boolean;
    myLogin: string | null;

    constructor(private router: Router, private myUserApi: MyUserApi,  private authService: AuthService  ) {
        this.menu = [
            {url: '/auth/login', active: false, name: 'Авторизация'},
            {url: '/auth/register', active: false, name: 'Регистрация'}
        ];


    }
    ngDoCheck() {
        this.logStatus = this.authService.isAuthenticated();
        this.myLogin = this.myUserApi.getCachedCurrent() && this.myUserApi.getCachedCurrent().name;
    }

    ngOnInit() {
        this.sub = this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.currentUrl = event.url;
            }
        });



    }

    logout() {
        this.authService.logout();
        this.logStatus = false;
        this.myLogin = '';
    }
    ngOnDestroy() {
        this.sub.unsubscribe();
    }



}
