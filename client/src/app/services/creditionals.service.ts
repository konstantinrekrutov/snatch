import { Injectable } from '@angular/core';
import {CacheService} from './cache.service';

@Injectable({
  providedIn: 'root'
})
export class CreditionalsService {
  constructor() {}
  static setCreditionalsId(creditionalsId) {
    localStorage.setItem('currentCreditionalsId', creditionalsId);
  }

  static getCreditionalsId() {
    return localStorage.getItem('currentCreditionalsId');
  }
}
