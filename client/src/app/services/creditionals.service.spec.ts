import { TestBed } from '@angular/core/testing';

import { CreditionalsService } from './creditionals.service';

describe('CreditionalsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreditionalsService = TestBed.get(CreditionalsService);
    expect(service).toBeTruthy();
  });
});
