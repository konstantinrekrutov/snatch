import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayRoutingModule } from './pay-routing.module';
import { HomeComponent } from './home/home.component';
import { OrderComponent } from './order/order.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [HomeComponent, OrderComponent],
  imports: [
    CommonModule,
    PayRoutingModule,
      FormsModule
  ]
})
export class PayModule { }
