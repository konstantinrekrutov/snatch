import {Component, OnInit} from '@angular/core';
import {MyUserApi} from '../../shared/sdk/services/custom';
import {ActivatedRoute} from '@angular/router';
import {Tarif, Wallet, UserTarif} from '../../shared/sdk/models';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
    tarif: Tarif;
    private walletAddress: string;
    private userTarif: UserTarif;

    constructor(private myUserApi: MyUserApi, private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.myUserApi.getOrderInfo(params['tid']).subscribe(orderInfo => {
                this.tarif = <Tarif>orderInfo['tarif'];
                this.walletAddress = orderInfo['wallet'];
                this.userTarif = <UserTarif>orderInfo['userTarif'];
            }, error1 => {
                console.error(error1);
            });
        });
    }

    check() {
        this.myUserApi.checkPay(this.tarif.id).subscribe(pay => {
            console.log(pay);
        }, error => {
            console.log(error);
        })
    }
}
