import { Component, OnInit } from '@angular/core';
import {TarifApi} from '../../shared/sdk/services/custom';
import {Tarif} from "../../shared/sdk/models";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  tarifs:Array<Tarif>;
  constructor(private tarifApi: TarifApi) { }

  ngOnInit() {
    this.tarifApi.find({where: {default: false}}).subscribe(tarifs => {
      this.tarifs = <Array<Tarif>>tarifs;
    });
  }

}
