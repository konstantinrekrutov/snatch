import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {OrderComponent} from './order/order.component';
import {AuthGuardService} from '../services/auth/auth-guard.service';

const routes: Routes = [
    {path: '', component: HomeComponent, pathMatch: 'full', canActivate: [AuthGuardService]},
    {path: 'order/:tid', component: OrderComponent, canActivate: [AuthGuardService]}
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PayRoutingModule {
}
