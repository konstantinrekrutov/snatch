import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationRoutingModule } from './application-routing.module';
import {MeComponent} from './me/me.component';
import { FormsModule } from '@angular/forms';

import { AddTelegramAccountComponent } from './add-telegram-account/add-telegram-account.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { TwoFaComponent } from './two-fa/two-fa.component';

@NgModule({
  declarations: [
    MeComponent,
    AddTelegramAccountComponent,
    SignInComponent,
    TwoFaComponent
  ],
  imports: [
    CommonModule,
    ApplicationRoutingModule,
      FormsModule
  ]
})
export class ApplicationModule { }
