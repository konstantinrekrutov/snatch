import {Component, OnInit} from '@angular/core';
import {CreditionalsApi} from '../../shared/sdk/services/custom';
import {Creditionals} from '../../shared/sdk/models';
import {NotifyService} from '../../services/notify.service';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import {CreditionalsService} from '../../services/creditionals.service';

@Component({
    selector: 'app-me',
    templateUrl: './me.component.html',
    styleUrls: ['./me.component.css']
})
export class MeComponent implements OnInit {
    creditionals: Array<Creditionals> = [];
    creditionalsNonApproved: Array<Creditionals> = [];
    creditionalsApproved: Array<Creditionals> = [];

    constructor(
        private creditionalsApi: CreditionalsApi, private authService: AuthService,
                private router: Router, private notify: NotifyService) {
    }

    ngOnInit() {
        this.getCreditionals();
    }

    private getCreditionals() {
        this.creditionalsApi.find({include: 'telegramUser'}).subscribe(creditionals => {
            this.creditionals = <Array<Creditionals>>creditionals;
            this.creditionals.map(creditional => {
                creditional.status < 3 ? this.creditionalsNonApproved.push(creditional) : this.creditionalsApproved.push(creditional);
            });
        }, error => {
            this.authService.logout();
        });
    }

    getLink(creditionals: Creditionals) {
        let action;
        switch (creditionals.status) {
            case 3 : {
                action = '/copy';
                break;
            }
            case 2 : {
                action = '/2fa';
                break;
            }
            default: {
                action = '/signIn';

            }
        }
        return creditionals.id + action;
    }

    deleteCreditionals(creditionals: Creditionals) {
        const confirmResult = confirm('Вы уверены?');
        if (confirmResult) {
            this.creditionalsApi.deleteCreditionals(creditionals.id).subscribe(data => {
                    // this.notify.notify(data);
                    console.log(data);
                    this.getCreditionals();
                },
                //         error => {
                //     console.log(error);
                // }
            );
        }

    }

    setCreditionals(creditional: Creditionals) {
        CreditionalsService.setCreditionalsId(creditional.id);
        this.router.navigate(['app/copy']);
    }
}
