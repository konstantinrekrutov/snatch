import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MeComponent} from './me/me.component';
import {AddTelegramAccountComponent} from './add-telegram-account/add-telegram-account.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {TwoFaComponent} from './two-fa/two-fa.component';

const routes: Routes = [{
    path: '',
    component: MeComponent,
    pathMatch: 'full'
}, {
    path: 'add-account',
    component: AddTelegramAccountComponent
}, {
    path: ':id/signIn',
    component: SignInComponent
}, {
    path: ':id/2fa',
    component: TwoFaComponent
}, {
    path: 'copy',
    loadChildren: './copy/copy.module#CopyModule'
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ApplicationRoutingModule {
}
