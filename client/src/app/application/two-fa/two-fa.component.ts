import { Component, OnInit } from '@angular/core';
import {Creditionals} from '../../shared/sdk/models';
import {CreditionalsApi} from '../../shared/sdk/services/custom';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-two-fa',
  templateUrl: './two-fa.component.html',
  styleUrls: ['./two-fa.component.scss']
})
export class TwoFaComponent implements OnInit {
  creditionals: Creditionals;
  code: string;

  constructor(private creditionalsApi: CreditionalsApi,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(async data => {
      this.creditionals = <Creditionals>await this.creditionalsApi.findById(data['id']).toPromise();
    });
  }
  auth() {
    // console.log(this.code);
    this.creditionalsApi.twoFa(this.creditionals.id, {code: this.code}).subscribe(data => {
      console.log(data);
      this.router.navigate(['/app/' + data['id'] + '/signIn']);
    }, error => {
      console.log(error);
    });
  }
}
