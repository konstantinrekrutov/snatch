import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AddTaskComponent } from './add-task/add-task.component';
import {CopyRoutingModule} from './copy.routing.module';
import {FormsModule} from '@angular/forms';
import { EditTaskComponent } from './edit-task/edit-task.component';

@NgModule({
  declarations: [HomeComponent, AddTaskComponent, EditTaskComponent],
  imports: [
    CommonModule,
      CopyRoutingModule,
      FormsModule

  ]
})
export class CopyModule { }
