import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AddTaskComponent} from './add-task/add-task.component';
import {EditTaskComponent} from "./edit-task/edit-task.component";

const routes: Routes = [{
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
}, {
    path: 'add-task',
    component: AddTaskComponent
}, {
    path: 'edit/:id',
    component: EditTaskComponent
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CopyRoutingModule {
}
