import {Component, OnInit} from '@angular/core';
import {CopyTaskApi} from '../../../shared/sdk/services/custom';
import {ActivatedRoute, Router} from '@angular/router';
import {CopyTask} from '../../../shared/sdk/models';
import {CreditionalsService} from '../../../services/creditionals.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    tasks: Map<string, CopyTask>;
    creditionalsId;

    constructor(private copyTaskApi: CopyTaskApi, private activatedRoute: ActivatedRoute,
               private router: Router) {
        this.tasks = new Map<string, CopyTask>();
    }

    ngOnInit() {
        this.creditionalsId = CreditionalsService.getCreditionalsId();
        if (!this.creditionalsId) {
            return this.router.navigate(['/app']);
        }
        this.copyTaskApi.find({where: {creditionalsId: this.creditionalsId}}).subscribe(data => {
            data.forEach((task: CopyTask) => {
                this.tasks.set(task.id, <CopyTask>task);
            });
        }, error => {
            console.error(error);
        });

    }

    delete(task: CopyTask) {
        this.copyTaskApi.deleteById(task.id).subscribe((data) => {
            console.log(data);
        }, error => {
            console.error(error);
        });
    }

    setStarted(task: CopyTask) {
        this.copyTaskApi.setStarted(task.id, !task.started).subscribe((data: CopyTask) => {
            this.tasks.set(data.id, data);
        }, error => {
            console.error(error);
        });
    }
}
