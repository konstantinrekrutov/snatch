import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CopyTask, Creditionals, Tarif, UserTarif} from '../../../shared/sdk/models';
import {CreditionalsApi, MyUserApi} from '../../../shared/sdk/services/custom';
import {CreditionalsService} from '../../../services/creditionals.service';
import {UserTarifApi} from "../../../shared/sdk/services/custom/UserTarif";

@Component({
    selector: 'app-add-task',
    templateUrl: './add-task.component.html',
    styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {
    task: CopyTask;
    permissions;
    userTarif: UserTarif;
    private creditionalsId: string;
    proxyCreditionals: Array<Creditionals>;
    nameKeys = ['first_name', 'last_name', 'username'];

    constructor(private creditionalsApi: CreditionalsApi,
                private router: Router, private userTarifApi: UserTarifApi, private myUserApi:MyUserApi) {
        this.permissions = {canSendRaw: false, canUseProxy: false};
        this.task = new CopyTask();

    }

    ngOnInit() {
        this.creditionalsId = CreditionalsService.getCreditionalsId();
        if (!this.creditionalsId) {
            return this.router.navigate(['/app']);
        }
        this.userTarifApi.findOne({where:{myUserId: this.myUserApi.getCurrentId()}}).subscribe(userTarif => {
            this.userTarif = <UserTarif>userTarif;
            this.setPermissions();
        }, error1 => {
            console.error(error1);
        });
        this.creditionalsApi.find({
            where: {id: {neq: this.creditionalsId}},
            include: 'telegramUser'
        }).subscribe(creditionals => {
            this.proxyCreditionals = <Array<Creditionals>>creditionals;
        }, error1 => {
            console.error(error1);
        });
    }

    setPermissions() {
        if (!this.userTarif) {
            return;
        }
        const now = new Date();
        if (new Date(this.userTarif.canSendRawTo).getTime() > now.getTime()) {
            this.permissions.canSendRaw = true;
        }
        if (new Date(this.userTarif.canUseProxyTo).getTime() > now.getTime()) {
            this.permissions.canUseProxy = true;
        }
    }

    getName(telegramUser) {
        const name = [];
        this.nameKeys.forEach(key => {
            if (telegramUser[key]) {
                name.push(telegramUser[key]);
            }
        });
        return name.join(' ');
    }

    searchUser(to) {
        const name = to ? this.task.to : this.task.from;
        const cid = this.task.creditionalsProxyId && to ? this.task.creditionalsProxyId : this.creditionalsId;
        this.creditionalsApi.searchUser(cid, name).subscribe(data => {
            console.log(data);
        });
    }

    addTask() {
        this.creditionalsApi.addCopyTask(this.creditionalsId, this.task).subscribe(data => {
            console.log(data);
        });
    }
}
