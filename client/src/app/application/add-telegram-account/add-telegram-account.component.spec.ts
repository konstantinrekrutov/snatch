import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTelegramAccountComponent } from './add-telegram-account.component';

describe('AddTelegramAccountComponent', () => {
  let component: AddTelegramAccountComponent;
  let fixture: ComponentFixture<AddTelegramAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTelegramAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTelegramAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
