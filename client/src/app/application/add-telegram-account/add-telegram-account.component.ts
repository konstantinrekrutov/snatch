import {Component, OnInit} from '@angular/core';
import {NotifyService} from '../../services/notify.service';
import {Router} from '@angular/router';
import {CreditionalsApi} from '../../shared/sdk/services/custom';
import {Creditionals} from '../../shared/sdk/models';

@Component({
    selector: 'app-add-telegram-account',
    templateUrl: './add-telegram-account.component.html',
    styleUrls: ['./add-telegram-account.component.scss']
})
export class AddTelegramAccountComponent implements OnInit {

    constructor(private creditionalsApi: CreditionalsApi,
                private notify: NotifyService, private router: Router) {

    }

    creditionals: Creditionals = new Creditionals();
    displayPreloader = false;

    ngOnInit() {
    }
    async sendCode() {
        this.displayPreloader = true;
        this.creditionalsApi.addNew(this.creditionals).subscribe(data => {
            this.notify.notify(data);
            this.router.navigate(['/app/' + data['id'] + '/signIn']);
        }, err => {
            this.displayPreloader = false;
            this.notify.notify(err);
        });
    }
}
