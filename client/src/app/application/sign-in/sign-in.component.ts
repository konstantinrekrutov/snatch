import {Component, OnInit} from '@angular/core';
import {CreditionalsApi} from '../../shared/sdk/services/custom';
import {Creditionals} from '../../shared/sdk/models';
import {ActivatedRoute, Router} from '@angular/router';
import {NotifyService} from '../../services/notify.service';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
    code;
    username;
    creditionals: Creditionals;
    displayPreloader = false;

    constructor(private creditionalsApi: CreditionalsApi,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private notify: NotifyService) {
    }

    signIn() {
        if (!this.code) {
            console.error('code error');
            return;
        }
        this.displayPreloader = true;
        this.creditionalsApi.signIn(this.creditionals.id, {code: this.code}).subscribe(data => {
            this.router.navigate(['/app']);
            this.displayPreloader = true;
            console.log(data);
        });

    }
    resendCode() {
        this.displayPreloader = true;
        this.creditionalsApi.sendCode(this.creditionals.id).subscribe(data => {
            this.displayPreloader = false;
            console.log(data);
            this.notify.notify({
                code: 'message_sent'
            });
        });

    }
    signUp() {
        if (!this.code) {
            console.log('code error');
            return;
        }
        this.creditionalsApi.signUp(this.creditionals.id, this.code).subscribe(data => {
            console.log(data);
        });

    }


    ngOnInit() {
        this.activatedRoute.params.subscribe(async data => {
            this.creditionals = <Creditionals>await this.creditionalsApi.findById(data['id']).toPromise();
        });
    }

}
