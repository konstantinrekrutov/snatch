import forEachObjIndexed from 'ramda/src/forEachObjIndexed';

import { generateID } from '../time-manager';
import blueDefer from '../../util/defer';

export class NetMessage {
  constructor(seq_no, body) {
    this.acked = false;
    this.msg_id = generateID();
    this.container = false;
    this.deferred = blueDefer();

    this.copyHelper = (value, key) => {
      //$FlowIssue
      this[key] = value;
    };

    this.seq_no = seq_no;
    this.body = body;
  }
  copyOptions(options) {
    //TODO remove this
    forEachObjIndexed(this.copyHelper, options);
  }

  size() {
    if (this.body instanceof Uint8Array) return this.body.byteLength;else return this.body.length;
  }
}

export class NetContainer extends NetMessage {
  constructor(seq_no, body, inner) {
    super(seq_no, body);
    this.container = true;
    this.inner = inner;
  }
}
//# sourceMappingURL=net-message.js.map