import Main from './index';

function MTProto(config = {}) {
  var mtproto = new Main(config);
  return mtproto.api;
}

export default MTProto;
//# sourceMappingURL=wrap.js.map