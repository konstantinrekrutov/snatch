import Promise from 'bluebird';

import has from 'ramda/src/has';
import pathEq from 'ramda/src/pathEq';
import allPass from 'ramda/src/allPass';

import httpClient from '../../http';
import { ErrorBadResponse, ErrorNotFound } from '../../error';
import { generateID } from '../time-manager';
import { WriteMediator, ReadMediator } from '../../tl';

var is404 = pathEq(['response', 'status'], 404);
var notError = allPass([has('message'), has('type')]);

function _ref(err) {
  var error = void 0;
  switch (true) {
    case is404(err):
      error = new ErrorNotFound(err);
      break;
    case notError(err):
      error = new ErrorBadResponse('', err);
      break;
    default:
      error = err;
  }
  return Promise.reject(error);
}

var SendPlain = ({ Serialization, Deserialization }) => {
  var onlySendPlainReq = (url, requestBuffer) => {
    var requestLength = requestBuffer.byteLength,
        requestArray = new Int32Array(requestBuffer);

    var header = Serialization();
    var headBox = header.writer;

    WriteMediator.longP(headBox, 0, 0, 'auth_key_id'); // Auth key
    WriteMediator.long(headBox, generateID(), 'msg_id'); // Msg_id
    WriteMediator.int(headBox, requestLength, 'request_length');

    var headerBuffer = headBox.getBuffer(),
        headerArray = new Int32Array(headerBuffer);
    var headerLength = headerBuffer.byteLength;

    var resultBuffer = new ArrayBuffer(headerLength + requestLength),
        resultArray = new Int32Array(resultBuffer);

    resultArray.set(headerArray);
    resultArray.set(requestArray, headerArray.length);

    var requestData = resultArray;
    // let reqPromise
    // try {
    var reqPromise = httpClient.post(url, requestData, {
      responseType: 'arraybuffer'
    });
    // } catch (e) {
    //   reqPromise = Promise.reject(new ErrorBadResponse(url, e))
    // }
    return Promise.props({ url, req: reqPromise });
  };

  var onlySendPlainErr = _ref;

  var onlySendPlainRes = ({ url, req }) => {
    if (!req.data || !req.data.byteLength) return Promise.reject(new ErrorBadResponse(url));
    var deserializer = void 0;
    try {
      deserializer = Deserialization(req.data, { mtproto: true });
      var ctx = deserializer.typeBuffer;
      ReadMediator.long(ctx, 'auth_key_id');
      ReadMediator.long(ctx, 'msg_id');
      ReadMediator.int(ctx, 'msg_len');
    } catch (e) {
      return Promise.reject(new ErrorBadResponse(url, e));
    }

    return deserializer;
  };

  var sendPlainReq = (url, requestBuffer) => onlySendPlainReq(url, requestBuffer).then(onlySendPlainRes, onlySendPlainErr);

  return sendPlainReq;
};

export default SendPlain;
//# sourceMappingURL=send-plain-req.js.map