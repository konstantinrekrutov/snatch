import Promise from 'bluebird';

import Logger from '../../util/log';
var debug = Logger([`request`]);

import { MTError } from '../../error';
import { delayedCall } from '../../util/smart-timeout';


class Request {
  constructor(config, method, params = {}) {
    this.initNetworker = () => {
      if (!this.config.networker) {
        var { getNetworker: _getNetworker, netOpts: _netOpts, dc: _dc } = this.config;
        return _getNetworker(_dc, _netOpts).then(this.saveNetworker);
      }
      return Promise.resolve(this.config.networker);
    };

    this.saveNetworker = networker => this.config.networker = networker;

    this.performRequest = () => this.initNetworker().then(this.requestWith);

    this.requestWith = networker => networker.wrapApiCall(this.method, this.params, this.config.netOpts).catch({ code: 303 }, this.error303).catch({ code: 420 }, this.error420);

    this.config = config;
    this.method = method;
    this.params = params;

    this.performRequest = this.performRequest.bind(this);
    //$FlowIssue
    this.error303 = this.error303.bind(this);
    //$FlowIssue
    this.error420 = this.error420.bind(this);
    this.initNetworker = this.initNetworker.bind(this);
  }

  error303(err) {
    var matched = err.type.match(/^(PHONE_MIGRATE_|NETWORK_MIGRATE_|USER_MIGRATE_)(\d+)/);
    if (!matched || matched.length < 2) return Promise.reject(err);
    var [,, newDcID] = matched;
    if (+newDcID === this.config.dc) return Promise.reject(err);
    this.config.dc = +newDcID;
    delete this.config.networker;
    /*if (this.config.dc)
      this.config.dc = newDcID
    else
      await this.config.storage.set('dc', newDcID)*/
    //TODO There is disabled ability to change default DC
    //NOTE Shouldn't we must reassign current networker/cachedNetworker?
    return this.performRequest();
  }
  error420(err) {
    var matched = err.type.match(/^FLOOD_WAIT_(\d+)/);
    if (!matched || matched.length < 2) return Promise.reject(err);
    var [, waitTime] = matched;
    console.error(`Flood error! It means that mtproto server bans you on ${waitTime} seconds`);
    return +waitTime > 60 ? Promise.reject(err) : delayedCall(this.performRequest, +waitTime * 1e3);
  }
}

export default Request;
//# sourceMappingURL=request.js.map