function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

import Promise from 'bluebird';
// import UpdatesManager from '../updates'

import isNil from 'ramda/src/isNil';
import is from 'ramda/src/is';
import propEq from 'ramda/src/propEq';
import has from 'ramda/src/has';
import pathSatisfies from 'ramda/src/pathSatisfies';
import complement from 'ramda/src/complement';

import Logger from '../../util/log';
var debug = Logger`api-manager`;

import Auth from '../authorizer';


import blueDefer from '../../util/defer';
import { dTime } from '../time-manager';
import { chooseServer } from '../dc-configurator';

import KeyManager from '../rsa-keys-manger';
import { AuthKeyError } from '../../error';

import { bytesFromHex, bytesToHex } from '../../bin';

import { switchErrors } from './error-cases';
import { delayedCall } from '../../util/smart-timeout';

import Request from './request';

var hasPath = pathSatisfies(complement(isNil));

var baseDcID = 2;

var Ln = (length, obj) => obj && propEq('length', length, obj);

export class ApiManager {
  constructor(config, tls, netFabric, { on, emit }) {
    var _this = this;

    this.cache = {
      uploader: {},
      downloader: {},
      auth: {},
      servers: {},
      keysParsed: {}
    };

    this.networkSetter = (dc, options) => (authKey, serverSalt) => {
      var networker = this.networkFabric(dc, authKey, serverSalt, options);
      this.cache.downloader[dc] = networker;
      return networker;
    };

    function* _ref2(dcID, options = {}) {
      // const isUpload = options.fileUpload || options.fileDownload
      // const cache = isUpload
      //   ? this.cache.uploader
      //   : this.cache.downloader

      var cache = _this.cache.downloader;
      if (!dcID) throw new Error('get Networker without dcID');

      if (has(dcID, cache)) return cache[dcID];

      var akk = `dc${dcID}_auth_key`;
      var ssk = `dc${dcID}_server_salt`;

      var dcUrl = _this.chooseServer(dcID, false);

      var networkSetter = _this.networkSetter(dcID, options);

      var authKeyHex = yield _this.storage.get(akk);
      var serverSaltHex = yield _this.storage.get(ssk);

      if (cache[dcID]) return cache[dcID];

      if (Ln(512, authKeyHex)) {
        if (!serverSaltHex || serverSaltHex.length !== 16) serverSaltHex = 'AAAAAAAAAAAAAAAA';
        var _authKey = bytesFromHex(authKeyHex);
        var _serverSalt = bytesFromHex(serverSaltHex);

        return networkSetter(_authKey, _serverSalt);
      }

      if (!options.createNetworker) throw new AuthKeyError();

      var auth = void 0;
      try {
        auth = yield _this.auth(dcID, _this.cache.auth, dcUrl);
      } catch (error) {
        return netError(error);
      }

      var { authKey, serverSalt } = auth;

      yield _this.storage.set(akk, bytesToHex(authKey));
      yield _this.storage.set(ssk, bytesToHex(serverSalt));

      return networkSetter(authKey, serverSalt);
    }

    this.mtpGetNetworker = (() => {
      var _ref = _asyncToGenerator(_ref2);

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    })();

    function* _ref6(method, params, options = {}) {
      var deferred = blueDefer();
      var rejectPromise = function (error) {
        var err = void 0;
        if (!error) err = { type: 'ERROR_EMPTY', input: '' };else if (!is(Object, error)) err = { message: error };else err = error;
        deferred.reject(err);

        if (!options.noErrorBox) {
          //TODO weird code. `error` changed after `.reject`?

          /*err.input = method
            err.stack =
            stack ||
            hasPath(['originalError', 'stack'], error) ||
            error.stack ||
            (new Error()).stack*/
          _this.emit('error.invoke', error);
        }
      };

      yield _this.initConnection();

      var requestThunk = function (waitTime) {
        return delayedCall(req.performRequest, +waitTime * 1e3);
      };

      var dcID = options.dcID ? options.dcID : yield _this.storage.get('dc');

      var networker = yield _this.mtpGetNetworker(dcID, options);

      var cfg = {
        networker,
        dc: dcID,
        storage: _this.storage,
        getNetworker: _this.mtpGetNetworker,
        netOpts: options
      };
      var req = new Request(cfg, method, params);

      function _ref4() {
        return networker;
      }

      function _ref5(networker) {
        req.config.networker = networker;
        return req.performRequest();
      }

      req.performRequest().then(deferred.resolve, function (error) {
        var deferResolve = deferred.resolve;
        var apiSavedNet = _ref4;
        var apiRecall = _ref5;
        console.error(dTime(), 'Error', error.code, error.type, baseDcID, dcID);

        return switchErrors(error, options, dcID, baseDcID)(error, options, dcID, _this.emit, rejectPromise, requestThunk, apiSavedNet, apiRecall, deferResolve, _this.mtpInvokeApi, _this.storage);
      }).catch(rejectPromise);

      return deferred.promise;
    }

    this.mtpInvokeApi = (() => {
      var _ref3 = _asyncToGenerator(_ref6);

      return function (_x2, _x3) {
        return _ref3.apply(this, arguments);
      };
    })();

    this.setUserAuth = (dcID, userAuth) => {
      var fullUserAuth = Object.assign({ dcID }, userAuth);
      this.storage.set({
        dc: dcID,
        user_auth: fullUserAuth
      });
      this.emit('auth.dc', { dc: dcID, auth: userAuth });
    };

    var {
      server,
      api,
      app: {
        storage,
        publicKeys
      },
      schema,
      mtSchema
    } = config;
    this.apiConfig = api;
    this.publicKeys = publicKeys;
    this.storage = storage;
    this.serverConfig = server;
    this.schema = schema;
    this.mtSchema = mtSchema;
    this.chooseServer = chooseServer(this.cache.servers, server);
    this.on = on;
    this.emit = emit;
    this.TL = tls;
    this.keyManager = KeyManager(this.TL.Serialization, publicKeys, this.cache.keysParsed);
    this.auth = Auth(this.TL, this.keyManager);
    this.networkFabric = netFabric(this.chooseServer);
    this.mtpInvokeApi = this.mtpInvokeApi.bind(this);
    this.mtpGetNetworker = this.mtpGetNetworker.bind(this);
    var apiManager = this.mtpInvokeApi;
    apiManager.setUserAuth = this.setUserAuth;
    apiManager.on = this.on;
    apiManager.emit = this.emit;
    apiManager.storage = storage;

    // this.updatesManager = UpdatesManager(apiManager)
    // apiManager.updates = this.updatesManager

    return apiManager;
  }

  initConnection() {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      if (!isAnyNetworker(_this2)) {
        var storedBaseDc = yield _this2.storage.get('dc');
        var baseDc = storedBaseDc || baseDcID;
        var opts = {
          dcID: baseDc,
          createNetworker: true
        };
        var networker = yield _this2.mtpGetNetworker(baseDc, opts);
        var nearestDc = yield networker.wrapApiCall('help.getNearestDc', {}, opts);
        var { nearest_dc, this_dc } = nearestDc;
        yield _this2.storage.set('dc', nearest_dc);
        debug(`nearest Dc`)('%O', nearestDc);
        if (nearest_dc !== this_dc) yield _this2.mtpGetNetworker(nearest_dc, { createNetworker: true });
      }
    })();
  }

  mtpClearStorage() {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      var saveKeys = [];
      for (var _dcID = 1; _dcID <= 5; _dcID++) {
        saveKeys.push(`dc${_dcID}_auth_key`);
        saveKeys.push(`t_dc${_dcID}_auth_key`);
      }
      _this3.storage.noPrefix(); //TODO Remove noPrefix

      var values = yield _this3.storage.get(...saveKeys);

      yield _this3.storage.clear();

      var restoreObj = {};
      saveKeys.forEach(function (key, i) {
        var value = values[i];
        if (value !== false && value !== undefined) restoreObj[key] = value;
      });
      _this3.storage.noPrefix();

      return _this3.storage.set(restoreObj); //TODO definitely broken
    })();
  }
}

var isAnyNetworker = ctx => Object.keys(ctx.cache.downloader).length > 0;

var netError = error => {
  console.log('Get networker error', error, error.stack);
  return Promise.reject(error);
};
//# sourceMappingURL=index.js.map