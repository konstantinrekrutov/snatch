function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

import Promise from 'bluebird';

import { WriteMediator } from '../tl';

import { bytesToHex, sha1BytesSync, bytesFromHex, strDecToHex } from '../bin';

export var KeyManager = (Serialization, publisKeysHex, publicKeysParsed) => {
  var prepareRsaKeys = (() => {
    var _ref = _asyncToGenerator(_ref2);

    return function prepareRsaKeys() {
      return _ref.apply(this, arguments);
    };
  })();

  var selectRsaKeyByFingerPrint = (() => {
    var _ref3 = _asyncToGenerator(_ref5);

    return function selectRsaKeyByFingerPrint(_x) {
      return _ref3.apply(this, arguments);
    };
  })();

  var prepared = false;

  var mapPrepare = ({ modulus, exponent }) => {
    var RSAPublicKey = Serialization();
    var rsaBox = RSAPublicKey.writer;
    WriteMediator.bytes(rsaBox, bytesFromHex(modulus), 'n');
    WriteMediator.bytes(rsaBox, bytesFromHex(exponent), 'e');

    var buffer = rsaBox.getBuffer();

    var fingerprintBytes = sha1BytesSync(buffer).slice(-8);
    fingerprintBytes.reverse();

    publicKeysParsed[bytesToHex(fingerprintBytes)] = {
      modulus,
      exponent
    };
  };

  function* _ref2() {
    if (prepared) return;

    yield Promise.map(publisKeysHex, mapPrepare);

    prepared = true;
  }

  function* _ref5(fingerprints) {
    yield prepareRsaKeys();

    var fingerprintHex = void 0,
        foundKey = void 0;
    for (var _iterator = fingerprints, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
      var _ref4;

      if (_isArray) {
        if (_i >= _iterator.length) break;
        _ref4 = _iterator[_i++];
      } else {
        _i = _iterator.next();
        if (_i.done) break;
        _ref4 = _i.value;
      }

      var fingerprint = _ref4;

      fingerprintHex = strDecToHex(fingerprint);
      foundKey = publicKeysParsed[fingerprintHex];
      if (foundKey) return Object.assign({ fingerprint }, foundKey);
    }
    return false;
  }

  return {
    prepare: prepareRsaKeys,
    select: selectRsaKeyByFingerPrint
  };
};

export default KeyManager;
//# sourceMappingURL=rsa-keys-manger.js.map