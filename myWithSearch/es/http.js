import axios from 'axios';

export var httpClient = axios.create();
delete httpClient.defaults.headers.post['Content-Type'];
delete httpClient.defaults.headers.common['Accept'];

export default httpClient;
//# sourceMappingURL=http.js.map