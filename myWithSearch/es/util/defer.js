import Promise from 'bluebird';
/**
 * Defered promise like in Q and $q
 *
 * @returns {{ resolve: (res: any) => void, reject: (res: any) => void, promise: Promise<{}> }}
 */
export var blueDefer = () => {
  var resolve = void 0,
      reject = void 0;
  var promise = new Promise((rs, rj) => {
    resolve = rs;
    reject = rj;
  });
  return {
    resolve,
    reject,
    promise
  };
};

export default blueDefer;
//# sourceMappingURL=defer.js.map