

// import memoize from 'memoizee'

import Debug from 'debug';

import trim from 'ramda/src/trim';
import map from 'ramda/src/map';
import chain from 'ramda/src/chain';
import pipe from 'ramda/src/pipe';
import split from 'ramda/src/split';
import both from 'ramda/src/both';
import is from 'ramda/src/is';
import when from 'ramda/src/when';
import take from 'ramda/src/take';
import reject from 'ramda/src/reject';
import isEmpty from 'ramda/src/isEmpty';
import join from 'ramda/src/join';
import unapply from 'ramda/src/unapply';
import unnest from 'ramda/src/unnest';
import tail from 'ramda/src/tail';

import dTime from './dtime';
import { immediate } from './smart-timeout';

var tagNormalize = e => `[${e}]`;

var arrify = unapply(unnest);

var fullNormalize = pipe(arrify, chain(split(',')), map(trim), reject(isEmpty), map(tagNormalize), join(''));

var stringNormalize = when(both(is(String), e => e.length > 50), take(150));
// const isSimple = either(
//   is(String),
//   is(Number)
// )

// const prettify = unless(
//   isSimple,
//   pretty
// )

var genericLogger = Debug('telegram-mtproto');

class LogEvent {
  constructor(log, values) {
    this.log = log;
    this.values = values;
  }
  print() {
    this.log(...this.values);
  }
}

class Sheduler {
  constructor() {
    this.queue = [];
    this.buffer = [];

    this.add = (log, time, tagStr, values) => {
      var results = values.map(stringNormalize);
      var first = results[0] || '';
      var other = tail(results);
      var firstLine = [tagStr, time, first].join('  ');
      this.buffer.push(new LogEvent(log, [firstLine, ...other]));
    };

    this.sheduleBuffer = () => {
      this.queue.push(this.buffer);
      this.buffer = [];
    };

    this.print = () => {
      for (var _iterator = this.queue, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
        var _ref;

        if (_isArray) {
          if (_i >= _iterator.length) break;
          _ref = _iterator[_i++];
        } else {
          _i = _iterator.next();
          if (_i.done) break;
          _ref = _i.value;
        }

        var buffer = _ref;

        for (var _iterator2 = buffer, _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
          var _ref2;

          if (_isArray2) {
            if (_i2 >= _iterator2.length) break;
            _ref2 = _iterator2[_i2++];
          } else {
            _i2 = _iterator2.next();
            if (_i2.done) break;
            _ref2 = _i2.value;
          }

          var logEvent = _ref2;

          logEvent.print();
        }
      }this.queue = [];
    };

    setInterval(this.sheduleBuffer, 50);
    setInterval(this.print, 300);
  }
}

var sheduler = new Sheduler();

var Logger = (moduleName, ...rest) => {
  var fullModule = arrify(moduleName, ...rest);
  fullModule.unshift('telegram-mtproto');
  var fullname = fullModule.join(':');
  var debug = Debug(fullname);
  var logger = tags => {
    var tagStr = fullNormalize(tags);
    return (...objects) => {
      var time = dTime();
      immediate(sheduler.add, debug, time, tagStr, objects);
    };
  };
  return logger;
};

export var setLogger = customLogger => {
  Debug.log = customLogger;
};

export default Logger;
//# sourceMappingURL=log.js.map