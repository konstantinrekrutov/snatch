import Promise from 'bluebird';

var cancelToken = Symbol('cancel token');

var timeoutRefs = new WeakSet();

var pause = delay => new Promise(r => setTimeout(r, delay));

export var smartTimeout = (fn, delay = 0, ...args) => {
  var newToken = Symbol('cancel id');
  var checkRun = () => {
    if (timeoutRefs.has(newToken)) {
      timeoutRefs.delete(newToken);
      return fn(...args);
    } else return false;
  };
  var promise = pause(delay).then(checkRun);
  promise[cancelToken] = newToken;
  return promise;
};

smartTimeout.cancel = promise => {
  if (!promise || !promise[cancelToken]) return false;
  var token = promise[cancelToken];
  return timeoutRefs.has(token) ? timeoutRefs.delete(token) : false;
};

export var immediate = (fn, ...args) => Promise.resolve().then(() => fn(...args));

export var delayedCall = (fn, delay = 0, ...args) => pause(delay).then(() => fn(...args));

smartTimeout.immediate = immediate;
smartTimeout.promise = delayedCall;

export default smartTimeout;
//# sourceMappingURL=smart-timeout.js.map