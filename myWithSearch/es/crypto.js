import when from 'ramda/src/when';
import is from 'ramda/src/is';
import identity from 'ramda/src/identity';
import has from 'ramda/src/has';
import both from 'ramda/src/both';
import isNode from 'detect-node';

import blueDefer from './util/defer';
import smartTimeout from './util/smart-timeout';
import { convertToUint8Array, sha1HashSync, sha256HashSync, aesEncryptSync, aesDecryptSync, convertToByteArray, convertToArrayBuffer, pqPrimeFactorization, bytesModPow } from './bin';

var convertIfArray = when(is(Array), convertToUint8Array);
var webWorker = !isNode;
var taskID = 0;
var awaiting = {};
var webCrypto = isNode ? false
//eslint-disable-next-line
: window.crypto.subtle || window.crypto.webkitSubtle //TODO remove browser depends
//eslint-disable-next-line
|| window.msCrypto && window.msCrypto.subtle;
var useWebCrypto = webCrypto && !!webCrypto.digest;
var useSha1Crypto = useWebCrypto;
var useSha256Crypto = useWebCrypto;
var finalizeTask = (taskID, result) => {
  var deferred = awaiting[taskID];
  if (deferred) {
    // console.log(rework_d_T(), 'CW done')
    deferred.resolve(result); //TODO Possibly, can be used as
    delete awaiting[taskID]; //
  } //    deferred = Promise.resolve()
}; //    deferred.resolve( result )

var isCryptoTask = both(has('taskID'), has('result'));

//eslint-disable-next-line
var workerEnable = !isNode && window.Worker;

function _ref(e) {
  if (e.data === 'ready') {
    console.info('CW ready');
  } else if (!isCryptoTask(e.data)) {
    console.info('Not crypto task', e, e.data);
    return e;
  } else return webWorker ? finalizeTask(e.data.taskID, e.data.result) : webWorker = tmpWorker;
}

function _ref2(error) {
  console.error('CW error', error, error.stack);
  webWorker = false;
}

if (workerEnable) {
  var TmpWorker = require('worker-loader?inline!./worker.js');
  var tmpWorker = new TmpWorker();
  // tmpWorker.onmessage = function(event) {
  //   console.info('CW tmpWorker.onmessage', event && event.data)
  // }
  tmpWorker.onmessage = _ref;

  tmpWorker.onerror = _ref2;
  tmpWorker.postMessage('b');
  webWorker = tmpWorker;
}

var performTaskWorker = (task, params, embed) => {
  // console.log(rework_d_T(), 'CW start', task)
  var deferred = blueDefer();

  awaiting[taskID] = deferred;

  params.task = task;
  params.taskID = taskID;(embed || webWorker).postMessage(params);

  taskID++;

  return deferred.promise;
};

function _ref3(digest) {
  return (
    // console.log(rework_d_T(), 'Native sha1 done')
    digest
  );
}

var sha1Hash = bytes => {
  function _ref4(e) {
    console.error('Crypto digest error', e);
    useSha1Crypto = false;
    return sha1HashSync(bytes);
  }

  if (useSha1Crypto) {
    // We don't use buffer since typedArray.subarray(...).buffer gives the whole buffer and not sliced one.
    // webCrypto.digest supports typed array
    var bytesTyped = convertIfArray(bytes);
    // console.log(rework_d_T(), 'Native sha1 start')
    return webCrypto.digest({ name: 'SHA-1' }, bytesTyped).then(_ref3, _ref4);
  }
  return smartTimeout.immediate(sha1HashSync, bytes);
};

var sha256Hash = bytes => {
  function _ref5(e) {
    console.error('Crypto digest error', e);
    useSha256Crypto = false;
    return sha256HashSync(bytes);
  }

  if (useSha256Crypto) {
    var bytesTyped = convertIfArray(bytes);
    // console.log(rework_d_T(), 'Native sha1 start')
    return webCrypto.digest({ name: 'SHA-256' }, bytesTyped).then(identity
    // console.log(rework_d_T(), 'Native sha1 done')
    , _ref5);
  }
  return smartTimeout.immediate(sha256HashSync, bytes);
};

var aesEncrypt = (bytes, keyBytes, ivBytes) => smartTimeout.immediate(() => convertToArrayBuffer(aesEncryptSync(bytes, keyBytes, ivBytes)));

var aesDecrypt = (encryptedBytes, keyBytes, ivBytes) => smartTimeout.immediate(() => convertToArrayBuffer(aesDecryptSync(encryptedBytes, keyBytes, ivBytes)));

var factorize = bytes => {
  bytes = convertToByteArray(bytes);
  return webWorker ? performTaskWorker('factorize', { bytes }) : smartTimeout.immediate(pqPrimeFactorization, bytes);
};

var modPow = (x, y, m) => webWorker ? performTaskWorker('mod-pow', {
  x,
  y,
  m
}) : smartTimeout.immediate(bytesModPow, x, y, m);

export var CryptoWorker = {
  sha1Hash,
  sha256Hash,
  aesEncrypt,
  aesDecrypt,
  factorize,
  modPow
};

export default CryptoWorker;
//# sourceMappingURL=crypto.js.map