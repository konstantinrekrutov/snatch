import Promise from 'bluebird';

import clone from 'ramda/src/clone';

export var ValueStore = () => {
  var val = null;

  return {
    get: () => clone(val),
    set: newVal => val = newVal
  };
};

export var ValueStoreMap = () => {
  var val = new Map();

  return {
    get: key => clone(val.get(key)),
    set: (key, newVal) => val.set(key, newVal)
  };
};

export var TimeOffset = ValueStore();
export var dcList = ValueStoreMap();

function _ref2() {
  return {};
}

export var AsyncStorage = () => {
  var store = new Map();

  var get = key => store.get(key);
  var set = (key, val) => store.set(key, val);

  function _ref(e) {
    return store.delete(e);
  }

  var remove = keys => keys.map(_ref);
  var clr = () => store.clear();
  return {
    get: key => Promise.resolve(get(key)),
    set: (key, val) => Promise.resolve(set(key, val)),
    remove: (...keys) => Promise.resolve(remove(keys)),
    clear: () => Promise.resolve(clr()),
    noPrefix: _ref2,
    store
  };
};

export var PureStorage = AsyncStorage(); /*{
                                         get     : (...keys) => new Promise(rs => ConfigStorage.get(keys, rs)),
                                         set     : obj => new Promise(rs => ConfigStorage.set(obj, rs)),
                                         remove  : (...keys) => new Promise(rs => ConfigStorage.remove(...keys, rs)),
                                         noPrefix: () => ConfigStorage.noPrefix(),
                                         clear   : () => new Promise(rs => ConfigStorage.clear(rs))
                                         }*/
//# sourceMappingURL=store.js.map