'use strict';
const EventEmitter = require('events');
const channelStates = {};

class UpdatesManager extends EventEmitter {
  constructor(client, creditionals, Helper) {
    super();
    this.Helper = Helper;
    this.tasks = {};

    this.creditionals = creditionals;
    this.loopStarted = false;
    this.stopped = false;
    this.initing = false;
    this.chatIds = [];
    this.client = client;
    this.init();
  }

  init() {
    if (this.initing) {
      return;
    }
    this.initing = true;
    this.client('updates.getState', {}, {noErrorBox: true}).then((stateResult) => {
      this.updatesState = {
        seq: stateResult.seq,
        pts: stateResult.pts,
        date: stateResult.date,
      };
      this.emit('stateInitialized');
      this.initing = false;
    });
  }

  newMessagesFromChannel(channelID, messages) {
    const isMe = parseInt(channelID) === parseInt(this.creditionals.telegramUserId);
    channelID = +channelID;
    if (this.chatIds.indexOf(channelID) !== -1 || (isMe && messages[0] &&  this.chatIds.indexOf(messages[0].from_id) !== -1)) {
      messages.sort((a, b) => {
        if (a.date > b.date) {
          return 1;
        } else if (a.date < b.date) {
          return -1;
        } else {
          return a.id > b.id ? 1 : -1;
        }
      });
      const last = messages.length - 1;
      if (messages[last].date >= this.updatesState.date) {
        this.updatesState.date = (messages[last].date + 1);
      }

      this.emit('newMessages', messages);
    }
  }

  getOrCreateUserData(tid, cid) {
    return new Promise((resolve, reject) => {
      this.Helper.getUserData(tid, cid).then(userData => {
        if (userData) {
          return resolve(userData);
        }
        this.Helper.app.models.Creditionals.findOne({where: {telegramUserId: tid}}).then(creditionals => {
          if (!creditionals) {
            return reject();
          }
          this.Helper.app.models.TelegramUser.renewContacts(this.client, creditionals, tid).then(() => {
            this.Helper.getUserData(tid, cid).then(userData => {
              if (userData) {
                return resolve(userData);
              } else {
                reject();
              }
            }, reject);
          }, reject);
        }, reject);
      }, reject);
    });
  }

  getChannelUpdateIfNeed(update) {
    return new Promise((resolve, reject) => {
      const channelID = update.channel_id;
      if (this.chatIds.indexOf(channelID) === -1) {
        return resolve();
      }
      if (!channelStates[channelID]) {
        return resolve();
      }
      this.getOrCreateUserData(this.creditionals.telegramUserId, channelID).then(userData => {
        if (!userData) {
          reject();
          return;
        }
        console.log('pts is ' + channelStates[channelID]);

        this.client('updates.getChannelDifference', {
          channel: {
            _: 'inputPeerChannel',
            channel_id: channelID,
            access_hash: userData.access_hash,
          },
          filter: {_: 'channelMessagesFilterEmpty'},
          pts: channelStates[channelID],
          limit: 30,
        }).then((diffs) => {
          channelStates[channelID] = diffs.pts;
          if (diffs._ === 'updates.channelDifferenceTooLong') {
            resolve();
            return;
          }
          if (diffs.messages) {
            this.newMessagesFromChannel(channelID, diffs.messages);

          }
          resolve();
        }, reject);
      });
    });
  }

  getDiffs() {
    console.log('getDiff', this.updatesState.date);
    return new Promise((resolve, reject) => {
      console.log(this.updatesState.pts);
      this.client('updates.getDifference', {
        pts: this.updatesState.pts,
        date: this.updatesState.date,
        qts: -1,
      }).then(differenceResult => {
        if (differenceResult._ === 'updates.differenceEmpty') {
          this.updatesState.date = differenceResult.date;
          this.updatesState.seq = differenceResult.seq;
          return resolve();
        }
        const nextState = differenceResult.intermediate_state || differenceResult.state;
        console.log('diffGeted', nextState.date);
        this.updatesState.pts = nextState.pts;
        this.updatesState.seq = nextState.seq;
        this.updatesState.date = nextState.date;
        const promises = [];
        const newMessages = {};
        differenceResult.other_updates.forEach((update) => {
          switch (update._) {
            case 'updateNewChannelMessage':
              const channelID = update.message.to_id.channel_id || update.message.to_id.chat_id;
              channelStates[channelID] = update.pts;
              if (!newMessages[channelID]) {
                newMessages[channelID] = [];
              }
              newMessages[channelID].push(update.message);
              break;
            case 'updateChannelTooLong':
              promises.push(this.getChannelUpdateIfNeed(update));
              break;
          }
        });

        differenceResult.new_messages.forEach((message) => {
          const channelID = message.to_id.channel_id || message.to_id.chat_id || message.to_id.user_id;
          if (!newMessages[channelID]) {
            newMessages[channelID] = [];
          }
          newMessages[channelID].push(message);

        });
        for (const i in newMessages) {
          this.newMessagesFromChannel(i, newMessages[i]);
        }

        if (promises.length) {
          Promise.all(promises).then(resolve, reject);
          return;
        }
        return resolve();
      }, reject);
    });
  }

  loop() {
    if (this.getting || this.stopped) {
      if(this.stopped) {
        this.updatesState = false;
      }
      return;
    }
    this.getting = true;
    this.getDiffs().then(() => {
      this.getting = false;
      setTimeout(() => {
        const now = Math.ceil(new Date().getTime() / 1000);
        const luft = (now - this.updatesState.date);
        if (luft >= 0) {
          this.loop();
        } else {
          setTimeout(() => {
            this.loop();
          }, ((luft+1)*1000))
        }
      }, 1000);
    }, (err) => {
      this.emit('error', err);
      this.getting = false;
    });
  }

  startChatsUpdates() {
    this.stopped = false;
    this.loopStarted = false;
    this.listenChatsUpdates();
  }
  stopChatsUpdates() {
    this.stopped = true;
    this.loopStarted = false;
  }

  listenChatsUpdates(tasks = false) {
    if (tasks) {
      this.tasks = {};
      this.chatIds = tasks.map(task => {
        if(!this.tasks[task.from]) {
          this.tasks[task.from] = {[task.to]:task}
        } else {
          this.tasks[task.from][task.to] = task;
        }
        return task.from;
      });
    }

    if (!this.updatesState) {
      this.init();
      this.once('stateInitialized', () => {
        this.listenChatsUpdates();
      });
    } else if (!this.loopStarted) {
      this.stopped = false;
      this.loopStarted = true;
      this.loop();
    }
  }
}

module.exports = UpdatesManager;
