const Bluebird = require('bluebird');

module.exports = class Storage {
    constructor(storage) {
        this.storage = storage;
    }

    get(key) {
        return Bluebird.resolve(this.storage.data[key]);
    }

    async set(key, val) {
        this.storage.data[key] = val;
        await this.storage.save()
        return Bluebird.resolve();
    }

    has(key) {
        return Bluebird.resolve(!!this.storage.data[key]);
    }

    async remove(...keys) {
        for (const key of keys) {
            delete this.storage.data[key];
        }
        return Bluebird.resolve(this.storage.save());
    }

    async clear() {
        this.storage.data = {};
        return Bluebird.resolve(this.storage.save());
    }
};