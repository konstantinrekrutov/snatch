'use strict';

module.exports = function (CopyTask) {

    CopyTask.checkLoop = function (from, to) {
        return new Promise(async (resolve, reject) => {
            try {
                const tasks = await CopyTask.find({where: {from: to}});
                if (!tasks.length) {
                    return resolve(false);
                }
                for (const i in tasks) {
                    const item = tasks[i];
                    if (item.to === from) {
                        return resolve(true);
                    }
                    const isLooped = await CopyTask.checkLoop(from, item.to);
                    if (isLooped) {
                        return resolve(true);
                    }
                }

                return resolve(false);
            } catch (e) {
                reject(e);
            }
        })
    };
    CopyTask.setStarted = function(taskId, started, cb) {
        const UserTarif = CopyTask.app.models.UserTarif;
        const Helper = CopyTask.app.models.Helper;
        CopyTask.findById(taskId, function(err, task) {
            if (err) {
                return cb(err);
            }
            if (!task) {
                return cb({message: 'not_found'});
            }
            if(task.last_message) {
                task.last_message = null;
            }
            if(started) {
                CopyTask.count({myUserId: task.myUserId, started: true}).then(count => {
                    if(count === 0) {
                        Helper.startTask(task).then(() => {
                            cb();
                        }, (err) => {
                            cb(err);
                        });
                        return;
                    }
                    UserTarif.findOne({myUserId: task.myUserId}).then((userTarif) => {
                        if (userTarif && userTarif.canSendRawTo && userTarif.taskCnt >= count) {
                            Helper.startTask(task).then(() => {
                                cb();
                            }, (err) => {
                                cb(err);
                            });


                        } else {
                            cb({statusCode:403, message:'limit'})
                        }
                    });
                });
            } else {
                Helper.stopTask(task).then(() => {
                    cb();
                }, (err) => {
                    cb(err);
                });

            }

        });
    };
    CopyTask.beforeRemote('deleteById', (ctx, obj, next) => {
        const Helper = CopyTask.app.models.Helper;

        CopyTask.findById(ctx.args.id, {include: 'creditionals'}, function(err, task) {
            if (task) {
                Helper.stopTask(task).then(() => {
                    next();
                }, err => {
                    next(err);
                });
            } else {
                if (!err) {
                    err = 'not_found';
                }
                next(err);
            }
        });
    });
    CopyTask.remoteMethod('setStarted', {
        accepts: [{
            arg: 'id',
            type: 'string',
            required: true,
        }, {arg: 'started', type: 'boolean', http: {source: 'body'}}],
        returns: {type: 'object', root: true},
        http: {path: '/:id/setStated'}
    });


};
