'use strict';
module.exports = function (TelegramUser) {

    TelegramUser.linkUsers = function (contacts, telegramUserId) {
        return new Promise((resolve, reject) => {
            TelegramUser.app.dataSources.mongo.connector.connect(async function (err, db) {
                let collection = db.collection('telegram_user_contacts');
                const links = [];
                const mapIdHash = {};

                contacts.forEach(contact => {
                    const id = telegramUserId + '' + contact.id;
                    mapIdHash[id] = contact.access_hash;
                    links.push({
                        contactId: contact.id,
                        telegramUserId: telegramUserId,
                        access_hash: contact.access_hash,
                        creator: contact.creator,
                        first_name: contact.first_name,
                        last_name: contact.last_name,
                        _id: id,
                    });
                });
                let issetObj = await collection.find({
                    _id: {
                        $in: links.map(link => {
                            return link._id;
                        }),
                    },
                }).toArray();


                let bulk;
                const replaceObj = [];
                const isset = issetObj.map(item => {
                    if (mapIdHash[item._id] && mapIdHash[item._id] !== item.access_hash) {
                        if (!bulk) {
                            bulk = collection.initializeUnorderedBulkOp()
                        }
                        bulk.findById(item._id).update({$set: {access_hash: mapIdHash[item._id]}});
                        item.access_hash = mapIdHash[item._id];
                        replaceObj.push(item);
                    }
                    return item._id;
                });
                if (bulk) {
                    await bulk.execute();
                }
                let newLinks;
                if (isset.length) {
                    newLinks = links.filter(item => {
                        return isset.indexOf(item._id) === -1;
                    });
                } else {
                    newLinks = links;
                }

                if (!newLinks.length) {
                    return resolve([]);
                }
                collection.insertMany(newLinks, function (err, res) {
                    if (err) {
                        return reject(err);
                    }
                    resolve(res.insertedIds);
                });
            });
        });

    };
    TelegramUser.addUsers = function (users) {

        return new Promise(function (resolve, reject) {
            TelegramUser.app.dataSources.mongo.connector.connect(async function (err, db) {
                let collection = db.collection('TelegramUser');
                let isset = await TelegramUser.find({
                    where: {
                        id: {
                            inq: users.map(user => {
                                return user.id;
                            }),
                        },
                    },
                });
                isset = isset.map(item => {
                    return item.id;
                });
                isset = isset ? isset : [];
                const newUsers = users.filter(item => {
                    return item._ !== 'chatForbidden' && isset.indexOf(item.id) === -1;
                }).map(user => {
                    user._id = user.id;
                    return user;
                });
                if (!newUsers.length) {
                    return resolve([]);
                }
                collection.insertMany(newUsers, function (err, res) {
                    if (err) {
                        return reject(err);
                    }
                    resolve(res.insertedIds);
                });
            });

        });

    };
    TelegramUser.addAndLink = async function (data, id) {
        try {
            await TelegramUser.addUsers(data);
            await TelegramUser.linkUsers(data, id);
        } catch (e) {
            console.error(e);
        }
    };
    TelegramUser.getAllMyChannels = async function (client, creditionals, id) {
        await TelegramUser.renewContacts(client, creditionals, id);
        return await TelegramUser.getChannels(id, null, true);
    };
    TelegramUser.getChannels = function (id, username, creator) {
        username = username ? username : false;
        return new Promise((resolve, reject) => {
            TelegramUser.app.dataSources.mongo.connector.connect(async (err, db) => {
                let collection = db.collection('telegram_user_contacts');
                let uname;
                if (username) {
                    uname = new RegExp(username, 'i');
                }
                const filter = {telegramUserId: id};
                if (creator) {
                    filter['creator'] = true;
                }
                const ids = await collection.find(filter).toArray();
                const where = {
                    and:
                        [
                            {
                                id: {
                                    inq: ids.map(item => {
                                        return item.contactId;
                                    })
                                }
                            },
                            {or: [{_: 'chat'}, {_: 'channel'}]}
                        ]
                };
                if (uname) {
                    where.and.push({title: uname});
                }
                TelegramUser.find({where: where}, function (err, res) {
                    if (err) {
                        return reject(err);
                    }
                    resolve(res);
                });
            });
        });
    }
    TelegramUser.prototype.getContacts = function (username) {
        return new Promise((resolve, reject) => {
            TelegramUser.app.dataSources.mongo.connector.connect(async (err, db) => {
                let collection = db.collection('telegram_user_contacts');
                const idsMap = {};
                const uname = new RegExp(username, 'i');
                const ids = await collection.find({telegramUserId: this.id}).toArray();
                const UserNameContacts = [];
                const userContacts = ids.filter((item => {
                    idsMap[item.contactId] = item;
                    const finded = (item.first_name && uname.test(item.first_name)) || (item.last_name && uname.test(item.last_name));
                    if (!finded) {
                        UserNameContacts.push(item.contactId);
                    }
                    return finded;
                }));
                const where = {
                    or:
                        [
                            {
                                and: [
                                    {
                                        id: {
                                            inq: UserNameContacts
                                        }
                                    },
                                    {or: [{username: uname}, {title: uname}]}
                                ]
                            },
                            {
                                id: {
                                    inq: userContacts.map(item => {
                                        return item.contactId;
                                    })
                                }
                            }

                        ]
                };
                TelegramUser.find({where: where}, function (err, res) {
                    if (err) {
                        return reject(err);
                    }
                    res.map(item => {
                        if (idsMap[item['id']]) {
                            item['first_name'] = idsMap[item['id']]['first_name'];
                            item['last_name'] = idsMap[item['id']]['last_name'];
                        }
                    });
                    resolve(res);
                });
            });
        });
    };
    TelegramUser.getUpdates = async function (client, state) {
        if (!state) {
            return false;
        }
        return await client('updates.getDifference', {
            pts: state.pts,
            date: state.date,
            qts: state.qts,
        });
    };
    TelegramUser.fillNewDialogs = function (client, id, lastDialog, offset_date) {
        return new Promise((resolve, reject) => {
            const options = {limit: 50};
            if (offset_date) {
                options['offset_date'] = offset_date;
            }
            TelegramUser.getDialogs(client, options).then(async data => {
                const lastId = data.messages.length - 1;
                let getNext = true;
                let hasNew;
                for (const i in data.messages) {
                    if (data.messages[i]['date'] <= lastDialog.date) {
                        getNext = false;
                        break;
                    }
                    hasNew = true;
                }
                if (hasNew) {
                    if (!offset_date && data.messages[0]) {
                        lastDialog.updateAttribute('date', data.messages[0].date, (err) => {
                            if (err) {
                                throw err;
                            }
                        });
                    }
                    data.chats && data.chats.length && await TelegramUser.addAndLink(data.chats, id);
                    data.users && data.users.length && await TelegramUser.addAndLink(data.users, id);
                }
                if (!data['hasNext'] || !getNext) {
                    return resolve();
                } else {
                    TelegramUser.fillNewDialogs(client, id, lastDialog, data.messages[lastId]['date']).then(resolve, reject);
                }
            }, reject);
        });
    };
    TelegramUser.getDialogs = function (client, options) {
        return new Promise((resolve, reject) => {
            options.limit = options.limit ? options.limit : 20;
            client('messages.getDialogs', options).then(async data => {
                data['hasNext'] = data.dialogs.length > 0;
                resolve(data);
            }, reject);
        });
    };

    TelegramUser.fillDialogs = function (client, id, creditionals, offset_date) {
        return new Promise((resolve, reject) => {
            const options = {limit: 100};
            if (offset_date) {
                options['offset_date'] = offset_date;
            }
            TelegramUser.getDialogs(client, options).then(async data => {
                const lastId = data.messages.length - 1;
                if (!options['offset_date']) {
                    await creditionals.lastDialog.create({count: data.count, date: data.messages[0]['date']});
                }
                data.chats && data.chats.length && await TelegramUser.addAndLink(data.chats, id);
                data.users && data.users.length && await TelegramUser.addAndLink(data.users, id);
                if (!data['hasNext']) {
                    return resolve();
                } else {
                    TelegramUser.fillDialogs(client, id, creditionals, data.messages[lastId]['date']).then(resolve, reject);
                }
            }, reject);
        });
    };
    TelegramUser.renewContacts = async function (client, creditionals, id) {
        let lastDialog = await creditionals.lastDialog.get();
        if (lastDialog) {
            await TelegramUser.fillNewDialogs(client, id, lastDialog);
        } else {
            await TelegramUser.fillDialogs(client, id, creditionals);
        }

    };
    TelegramUser.prototype.findChannels = async function (client, username, creditionals) {
        await TelegramUser.renewContacts(client, creditionals, this.id);
        const users = await TelegramUser.getChannels(id, username);
        return {users: users};
    };
    TelegramUser.prototype.findContacts = async function (client, username, creditionals) {
        await TelegramUser.renewContacts(client, creditionals, this.id);
        const users = await this.getContacts(username);
        return {users: users};
    };
};
