'use strict';

const crypto = require('crypto');

module.exports = function(Creditionals) {
  Creditionals.validatesUniquenessOf('api_id');
  Creditionals.createClient = function(creditionals) {
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;
    return new Promise(async function(resolve, reject) {
      try {
        creditionals = await Creditionals.create(creditionals);
        const client = await TelegramApiAdapter.getClient(creditionals);
        resolve({client, creditionals});
      } catch (e) {
        if (e.name !== 'ValidationError' && creditionals.id) {
          await TelegramApiAdapter.deleteClient(creditionals);
        }
        reject(e);
      }
    });
  };
  Creditionals.signUp = function(creditionalsId, code, cb) {
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;

    Creditionals.findById(creditionalsId, async function(err, creditionals) {
      if (err || !creditionals) {
        return cb(err);
      }
      const client = await TelegramApiAdapter.getClient(creditionals);
      let res;
      try {
        res = await client('auth.signUp', {
          phone_number: creditionals.phone,
          phone_code_hash: creditionals.phone_code_hash,
          phone_code: code,
          first_name: 'Raf',
          last_name: 'Rick',
        });
        cb(null, res);
      } catch (error) {
        if (error.type !== 'SESSION_PASSWORD_NEEDED') {
          return cb(error);
        }
        creditionals.status = 2;
        await creditionals.save();
        cb(error);
      }
    });
  };
  Creditionals.deleteCreditionals = function(creditionalsId, cb) {
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;
    Creditionals.findById(creditionalsId, function(err, creditionals) {
      if (err) {
        return cb(err);
      }
      if (!creditionals) {
        return cb({error: {message: 'not_found'}});
      }
      TelegramApiAdapter.deleteClient(creditionals).then(function(res) {
        return cb(null, res);
      }, err => {
        return cb(err);
      });
    });
  };
  Creditionals.signIn = function(creditionalsId, body, cb) {
    const code = body.code;
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;

    Creditionals.findById(creditionalsId, async function(err, creditionals) {
      if (err || !creditionals) {
        return cb(err);
      }
      const client = await TelegramApiAdapter.getClient(creditionals);
      let res;
      try {
        res = await client('auth.signIn', {
          phone_number: creditionals.phone,
          phone_code_hash: creditionals.phone_code_hash,
          phone_code: code,
        });
        creditionals.status = 3;
        let telegramUser = await creditionals.telegramUser.get();
        if (!telegramUser) {
          telegramUser = await Creditionals.app.models.TelegramUser.findById(res.user.id);
          if (telegramUser) {
            creditionals.telegramUserId = telegramUser.id;
          }
        }
        if (!telegramUser) {
          telegramUser = await creditionals.telegramUser.create(res.user);
        }
        await creditionals.save();
        cb(null, telegramUser);
      } catch (error) {
        if (error.type !== 'SESSION_PASSWORD_NEEDED') {
          return cb(error);
        }
        creditionals.status = 2;
        await creditionals.save();
        cb(error);
      }
    });
  };
  Creditionals.sendCode = function(creditionalsId, cb) {
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;

    Creditionals.findById(creditionalsId, async function(err, creditionals) {
      try {
        const client = await TelegramApiAdapter.getClient(creditionals);
        await creditionals.sendCode(client, true);
        cb(null, creditionals);
      } catch (e) {
        cb(e);
      }

    });
  };
  Creditionals.twoFa = function(creditionalsId, body, cb) {
    const password = body.code;
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;

    Creditionals.findById(creditionalsId, async function(err, creditionals) {
      try {

        const client = await TelegramApiAdapter.getClient(creditionals);
        const {current_salt} = await client('account.getPassword', {});
        const password_hash = crypto.createHash('sha256')
          .update(Buffer.concat([Buffer.from(current_salt), Buffer.from(password, 'utf8'), Buffer.from(current_salt)]))
          .digest();
        const res = await client('auth.checkPassword', {
          password_hash,
        });
        creditionals.status = 3;
        let telegramUser = await creditionals.telegramUser.get();
        if (!telegramUser) {
          telegramUser = await Creditionals.app.models.TelegramUser.findById(res.user.id);
          if (telegramUser) {
            creditionals.telegramUserId = telegramUser.id;
          }
        }
        if (!telegramUser) {
          telegramUser = await creditionals.telegramUser.create(res.user);
        }
        await creditionals.save();
        cb(null, telegramUser);
      } catch (e) {
        cb(e);
      }
    });
  };
  Creditionals.addNew = function(creditionals, options, cb) {
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;
    const token = options && options.accessToken;
    creditionals.myUserId = token && token.userId;

    Creditionals.createClient(creditionals).then((data) => {
      data.creditionals.sendCode(data.client).then(function(result) {
        return cb(null, result);
      }, (error) => {
        TelegramApiAdapter.deleteClient(data.creditionals).then(function() {
          return cb(error);
        }, error2 => {
          return cb({errors: [error, error2]});
        });
      });
    }, error => {
      return cb(error);
    });
  };

  Creditionals.getAllMyChannels = function(creditionalsId, cb) {
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;
    Creditionals.findById(creditionalsId, {include: 'telegramUser'}, async function(err, creditionals) {
      if (creditionals.status !== 3) {
        return cb({status: 403, message: 'not_auth'});
      }
      if (err) {
        return cb(err);
      }
      if (!creditionals) {
        return cb({message: 'creditionals_not_found'});
      }
      const telegramUser = await creditionals.telegramUser.get();
      if (!telegramUser) {
        return cb({status: 403, message: 'not_auth'});
      }
      TelegramApiAdapter.getClient(creditionals).then(async(client) => {
        Creditionals.app.models.TelegramUser.getAllMyChannels(client, creditionals, telegramUser.id).then(channels => {
          cb(null, channels);
        }, err => {
          cb(err);
        });
      });
    });
  };
  Creditionals.search = function(creditionalsId, username, type, cb) {
    const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;

    Creditionals.findById(creditionalsId, {include: 'telegramUser'}, async function(err, creditionals) {
      if (creditionals.status !== 3) {
        return cb({status: 403, message: 'not_auth'});
      }
      if (err) {
        return cb(err);
      }
      if (!creditionals) {
        return cb({message: 'creditionals_not_found'});
      }
      const telegramUser = await creditionals.telegramUser.get();
      if (!telegramUser) {
        return cb({status: 403, message: 'not_auth'});
      }
      TelegramApiAdapter.getClient(creditionals).then(async(client) => {
        let users;
        if (type === 'channel') {
          users = await telegramUser.findChannels(client, username, creditionals);
        } else {
          users = await telegramUser.findContacts(client, username, creditionals);
        }

        cb(null, users);
      }, error => {
        error.status = 400;
        cb(error);
      }).catch(err => {
        err.status = 400;
        cb(err);
      });

    });
  };
  Creditionals.searchChannel = function(creditionalsId, username, cb) {
    return Creditionals.search(creditionalsId, username, 'channel', cb);
  };
  Creditionals.searchUser = function(creditionalsId, username, cb) {
    return Creditionals.search(creditionalsId, username, 'all', cb);
  };
  Creditionals.saveCopyTask = function(creditionalsId, task, options, cb) {
    if (task.to === task.from) {
      return cb({status: 400, message: 'from==to'});
    }
    const CopyTask = Creditionals.app.models.CopyTask;
    const token = options && options.accessToken;
    task.myUserId = token && token.userId;
    Creditionals.findById(creditionalsId, async function(err, creditionals) {
      if (err) {
        err.status = 400;
        return cb(err);
      }
      if (!creditionals) {
        return cb({status: 400, message: 'creditionals_not_found'});
      }
      const isLooped = await CopyTask.checkLoop(+task.from, +task.to);
      if (isLooped) {
        return cb({status: 400, message: 'looped'});
      }
      creditionals.copyTasks.findById(task.id, function(err, oldTask) {
        if (err) {
          err.status = 400;
          return cb(err);
        }
        if (!oldTask) {
          return cb({status: 500, message: 'error_editing_task'});
        }
        const oldTaskStarted = oldTask.started;
        oldTask.updateAttributes(task).then((updatedTask) => {
          const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;
          const Helper = Creditionals.app.models.Helper;
          if (updatedTask.started) {
            Helper.updateTask(oldTask, updatedTask).then(() => {
              return cb(null, updatedTask);
            }, cb);
          } else if (oldTaskStarted && !updatedTask.started) {
            Helper.stopTask(oldTask, true).then(() => {
              return cb(null, updatedTask);
            }, cb);
          } else {
            return cb(null, updatedTask);
          }
        },cb);
      });
    });
  };
  Creditionals.addCopyTask = function(creditionalsId, task, options, cb) {
    if (task.to === task.from) {
      return cb({status: 400, message: 'from==to'});
    }
    const CopyTask = Creditionals.app.models.CopyTask;
    const token = options && options.accessToken;
    task.myUserId = token && token.userId;
    Creditionals.findById(creditionalsId, async function(err, creditionals) {
      if (err) {
        err.status = 400;
        return cb(err);
      }
      if (!creditionals) {
        return cb({status: 400, message: 'creditionals_not_found'});
      }

      const isLooped = await CopyTask.checkLoop(+task.from, +task.to);
      if (isLooped) {
        return cb({status: 400, message: 'looped'});
      }
      creditionals.copyTasks.create(task, function(err, addedTask) {
        if (err) {
          err.status = 400;
          return cb(err);
        }
        if (!addedTask) {
          return cb({status: 500, message: 'error_adding_task'});
        }
        if (addedTask.started) {
          const TelegramApiAdapter = Creditionals.app.models.TelegramApiAdapter;
          const Helper = Creditionals.app.models.Helper;

          TelegramApiAdapter.getClient(creditionals).then(client => {
            if (Helper.addTask(client, creditionals, addedTask)) {
              return cb(null, addedTask);
            }
            addedTask.started = false;
            addedTask.save().then(() => {
              cb('cant start');
            }, () => {
              cb('cant start but started');
            });
          });
        } else {
          return cb(null, addedTask);
        }
      });
    });
  };

  Creditionals.prototype.sendCode = function(client, sms) {
    return new Promise(async(resolve, reject) => {
      try {

        const {phone_code_hash} = await client('auth.sendCode', {
          phone_number: this.phone,
          current_number: false,
          api_id: this.api_id,
          api_hash: this.api_hash,
        });
        // if (sms) {
        //     await client('auth.sendSms', {
        //         phone_number: this.phone,
        //         phone_code_hash: phone_code_hash
        //     });
        // }
        this.phone_code_hash = phone_code_hash;
        this.status = 1;
        this.save(function(err, creditionals) {
          if (err) {
            return reject(err);
          }
          return resolve(creditionals);
        });
      } catch (e) {
        reject(e);
      }

    });
  };
  Creditionals.remoteMethod('getAllMyChannels', {
    accepts: [
      {arg: 'id', type: 'string', required: true},
    ],
    returns: {type: 'object', root: true},
    http: {path: '/:id/getAllMyChannels', verb: 'get'},
  });
  Creditionals.remoteMethod('searchChannel', {
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'username', type: 'string'},
    ],
    returns: {type: 'object', root: true},
    http: {path: '/:id/searchChannel', verb: 'get'},
  });
  Creditionals.remoteMethod('searchUser', {
    accepts: [
      {arg: 'id', type: 'string', required: true},
      {arg: 'username', type: 'string'},
    ],
    returns: {type: 'object', root: true},
    http: {path: '/:id/searchUser', verb: 'get'},
  });
  Creditionals.remoteMethod('addCopyTask', {
    accepts: [
      {arg: 'id', type: 'string', required: true, http: {source: 'path'}},
      {arg: 'task', type: 'object', http: {source: 'body'}},
      {
        'arg': 'options',
        'type': 'object',
        'http': 'optionsFromRequest',
      },
    ],
    returns: {type: 'object', root: true},
    http: {path: '/:id/addCopyTask'},
  });
  Creditionals.remoteMethod('saveCopyTask', {
    accepts: [
      {arg: 'id', type: 'string', required: true, http: {source: 'path'}},
      {arg: 'task', type: 'object', http: {source: 'body'}},
      {
        'arg': 'options',
        'type': 'object',
        'http': 'optionsFromRequest',
      },
    ],
    returns: {type: 'object', root: true},
    http: {path: '/:id/saveCopyTask'},
  });
  Creditionals.remoteMethod('deleteCreditionals', {
    accepts: {arg: 'id', type: 'string', required: true},
    returns: {type: 'object', root: true},
    http: {'path': '/:id/deleteCreditionals', 'verb': 'del'},
  });
  Creditionals.remoteMethod('addNew', {
    accepts: [{arg: 'creditionals', type: 'object', http: {source: 'body'}}, {
      'arg': 'options',
      'type': 'object',
      'http': 'optionsFromRequest',
    }],
    returns: {type: 'object', root: true},
  });

  Creditionals.remoteMethod('signIn', {
    accepts: [{
      arg: 'id',
      type: 'string',
      required: true,
      http: {source: 'path'},
    }, {
      arg: 'body',
      type: 'object',
      http: {source: 'body'},
    }],
    returns: {type: 'object', root: true},
    http: {'path': '/:id/signIn'},
  });
  Creditionals.remoteMethod('twoFa', {
    accepts: [{
      arg: 'id',
      type: 'string',
      required: true,
      http: {source: 'path'},
    }, {
      arg: 'body',
      type: 'object',
      http: {source: 'body'},
    }],
    returns: {type: 'object', root: true},
    http: {'path': '/:id/twoFa'},
  });
  Creditionals.remoteMethod('signUp', {
    accepts: [{arg: 'id', type: 'string', required: true}, {
      arg: 'code',
      type: 'string',
    }],
    returns: {type: 'object', root: true},
    http: {path: '/:id/signUp'},

  });
  Creditionals.remoteMethod('sendCode', {
    accepts: {arg: 'id', type: 'string', required: true},
    returns: {type: 'object', root: true},
    http: {path: '/:id/sendCode'},
  });
};
