'use strict';

const DEFAULT_RESET_PW_TTL = 15 * 60; // 15 mins in seconds
const senderAddress = 'karliksmith@ya.ru';
module.exports = function(MyUser) {
  MyUser.resetPasswordSend = function(options, cb) {
    const email = options.email.toLowerCase();
    const UserModel = this;
    const ttl = this.settings.resetPasswordTokenTTL || DEFAULT_RESET_PW_TTL;
    if (typeof email !== 'string') {
      const err = new Error('Email is required');
      err.statusCode = 400;
      err.code = 'EMAIL_REQUIRED';
      return cb(err);
    }
    const where = {email: email};
    UserModel.findOne({where: where}, function(err, user) {
      if (err) {
        return cb(err);
      }
      if (!user) {
        return cb({status: 404, code: 'user_not_found'});
      }
      if (UserModel.settings.emailVerificationRequired && !user.emailVerified) {
        return cb({status: 403, code: 'email_not_verified'});
      }
      user.createAccessToken(ttl, onTokenCreated);

      function onTokenCreated(err, accessToken) {
        if (err) {
          return cb(err);
        }
        MyUser.app.models.Email.send({
          to: email,
          from: senderAddress,
          subject: 'Восстановление пароля',
          html: 'Для восстановления пароля перейди по <a href="http://localhost:4200/auth/password-reset/' + accessToken.id + '">ссылке</a>',
        }, function(err) {
          if (err) {
            if (err.responseCode === 554) {
              return cb({status: 554, code: 'message_go_spam'});
            } else {
              return cb({status: 500, code: 'message_send_error'});
            }
          }
          cb(null, {code: 'verify_message_sent'});
        });
      }
    });
  };
  MyUser.resetMyPassword = function(options, cb) {
    const MyAccessToken = MyUser.app.models.MyAccessToken;
    MyAccessToken.resolve(options.access_token, (err, token) => {
      if (err || !token) {
        const error = new Error('token_invalid');
        error.status = 400;
        return cb(error);
      }
      token.validate(function(err, isValid) {
        if (err || !isValid) {
          const error = new Error('token_invalid');
          error.status = 400;
          return cb(error);
        }
        MyUser.findById(token.userId, function(err, myUser) {
          myUser.updateAttribute('password', options.newPassword,
            (err, newPassword) => {
              if (err) {
                const error = new Error('password_change_error');
                error.status = 500;
                return cb(error);
              }
              return cb(null, {code: 'password_changed'});
            });
        });
      });
    });
  };
  MyUser.prototype.verify = function(options, cb) {
    const user = this;
    MyUser.generateVerificationToken(user, {}, function(err, token) {
      user.verificationToken = token;
      user.save(function(err) {
        if (err) {
          return cb(err, null);
        }
        options.html = options.html.replace('{token}', token);
        options.html = options.html.replace('{uid}', user.id);
        MyUser.app.models.Email.send({
          to: options.to,
          from: options.from,
          subject: options.subject,
          html: options.html,
        }, function(err) {
          if (err) return cb(err, null);
          cb();
        });
      });
    });
  };

  MyUser.resend = function(options, cb) {
    const email = options.email.toLowerCase();
    MyUser.findOne({where: {email: email}}, function(err, user) {
      if (err) {
        return cb({code: 'server_error', status: 500});
      }
      if (!user) {
        const error = new Error('user_not_found');
        error.status = 404;
        return cb(error);
      }
      if (user.emailVerified) {
        const error = new Error('email_already_confirmed');
        error.status = 403;
        return cb(error);
      }
      const options = verifyEmailOptions(user);
      user.verify(options, function(err, response) {
        if (err) {
          MyUser.deleteById(user.id);
          const error = new Error('server_error');
          error.status = 500;
          return cb(error);
        }
        cb(null, {code: 'verify_message_sent'});
      });
    });
  };
  MyUser.orderPayed = function(myUserId, tarif, wallet) {
    const UserTarif = MyUser.app.models.UserTarif;
    const Order = MyUser.app.models.Order;
    Order.create({
      myUserId,
      tarifId: tarif.id,
      price: tarif.price,
      walletId: wallet.id,
      date: new Date(),
      days: tarif.days,
    }, (err) => {
      console.error(err);
    });
    return new Promise((resolve, reject) => {
      UserTarif.findOne({where: {myUserId}}).then(async userTarif => {
        const limitsCounts = await tarif.limitCounts.get();
        const now = new Date();
        const timePlus = tarif.days * 86400000;
        if (!userTarif) {
          const to = new Date(now.getTime() + timePlus);
          const ut = {};
          if (tarif.canSendRaw) {
            ut.canSendRawTo = to;
          }
          ut.taskCnt = limitsCounts.CopyTask;
          if (tarif.canUseProxy) {
            ut.canUseProxyTo = to;
          }
          ut.myUserId = myUserId;
          UserTarif.create(ut).then(resolve, reject);
        } else {
          if (tarif.canSendRaw) {
            if(userTarif.canSendRawTo.getTime() < now.getTime()) {
              userTarif.canSendRawTo = new Date(now.getTime() + timePlus);
            } else {
              userTarif.canSendRawTo =
                new Date(userTarif.canSendRawTo.getTime() + timePlus);
            }
          }
          if (tarif.canUseProxy) {
            if(userTarif.canUseProxyTo.getTime() < now.getTime()) {
              userTarif.canUseProxyTo = new Date(now.getTime() + timePlus);
            } else {
              userTarif.canUseProxyTo =
                new Date(userTarif.canUseProxyTo.getTime() + timePlus);
            }
          }
          userTarif.taskCnt = limitsCounts.CopyTask;
          userTarif.save().then(resolve, reject);
        }
      }, reject);
    });
  };
  MyUser.checkPay = function(tarifId, req, cb) {
    const Tarif = MyUser.app.models.Tarif;
    const Wallet = MyUser.app.models.Wallet;
    const myUserId = req.accessToken.userId;
    Tarif.findById(tarifId).then((tarif) => {
      Wallet.getWallet(tarif, myUserId).then(wallet => {
        Wallet.checkPay(wallet, tarif).then(() => {
          MyUser.orderPayed(myUserId, tarif, wallet).then(res => {
            cb(null, res);
          }, err => {
            cb(err);
          });
        }, (payedPrice) => {
          return cb(null, {payedPrice});
        });
      }, error => {
        cb(error);
      });
    }, cb);
  };
  MyUser.getOrderInfo = function(tarifId, req, cb) {
    const Tarif = MyUser.app.models.Tarif;
    const Wallet = MyUser.app.models.Wallet;
    const UserTarif = MyUser.app.models.UserTarif;
    const myUserId = req.accessToken.userId;
    const promises = [];
    Tarif.findById(tarifId).then((tarif) => {
      promises.push(tarif.userTarifs.find({where: {myUserId}}));
      promises.push(Wallet.getOrCreateWallet(tarif, myUserId));
      Promise.all(promises).then(res => {
        cb(null, {tarif: tarif, userTarif: res[0], wallet: res[1]});
      }, err => {
        cb(err);
      });
    }, cb);
  };
  MyUser.getMyRole = function(req, res, cb) {
    const RoleMapping = MyUser.app.models.RoleMapping;
    const Role = MyUser.app.models.Role;
    Role.getRoles(
      {
        principalType: RoleMapping.USER,
        principalId: req.accessToken.userId,
      }, {returnOnlyRoleNames: true}, (err, roles) => {
        if (err) {
          return cb(err);
        }
        cb(null, roles);
      });
  };
  MyUser.remoteMethod('resend', {
    accepts: {arg: 'options', type: 'object', http: {source: 'body'}},
    returns: {type: 'object', root: true},
  });
  MyUser.remoteMethod('getOrderInfo', {
    accepts: [{arg: 'tarifId', type: 'string'},
      {arg: 'req', type: 'object', 'http': {source: 'req'}}],
    returns: {type: 'object', root: true},
    http: {verb: 'get'},
  });
  MyUser.remoteMethod('getMyRole', {
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http': {source: 'res'}},
    ],
    returns: {type: 'object', root: true},
  });
  MyUser.remoteMethod('resetPasswordSend', {
    accepts: {arg: 'options', type: 'object', http: {source: 'body'}},
    returns: {type: 'object', root: true},
  });

  MyUser.remoteMethod('checkPay', {
    accepts: [{arg: 'tarifId', type: 'string'},
      {arg: 'req', type: 'object', 'http': {source: 'req'}}],
    returns: {type: 'object', root: true},
    http: {verb: 'get'},
  });
  MyUser.remoteMethod('resetMyPassword', {
    accepts: {arg: 'options', type: 'object', http: {source: 'body'}},
    returns: {type: 'object', root: true},
  });

  MyUser.afterRemote('confirm', function(context, user, next) {
    context.result = {code: 'confirmed'};
    next();
  });

  MyUser.beforeRemote('login', (context, modelInstance, next) => {
    context.args.credentials.email = context.args.credentials.email
      .toLowerCase();
    next();
  });
  MyUser.afterRemoteError('login', function(context, next) {
    const email = context.args.credentials.email;
    MyUser.findOne({where: {email: email}}, function(err, user) {
      if (err || !user) {
        next({status: 401, code: 'login_failed'});
      } else {
        if (!user.emailVerified) {
          next({status: 403, code: 'email_not_verified'});
        } else {
          next({status: 401, code: 'login_failed'});
        }
      }
    });
  });

  MyUser.afterRemoteError('confirm', function(context, next) {
    switch (context.error.statusCode) {
      case 404: {
        next({status: context.error.statusCode, code: 'user_not_found'});
        break;
      }
      case 400: {
        next({status: context.error.statusCode, code: 'token_invalid'});
        break;
      }
      default: {
        next({status: context.error.statusCode, code: 'server_error'});
      }
    }
  });
  MyUser.afterRemoteError('create', function(context, next) {
    let code;
    switch (context.error.statusCode) {
      case 422 : {
        code = 'email_exists';
        break;
      }
      case 554 : {
        code = 'message_go_spam';
        break;
      }
      default : {
        code = 'register_error';
        break;
      }
    }
    next({code, status: context.error.statusCode});
  });
  MyUser.observe('before save', function(ctx, next) {
    if (!ctx.isNewInstance) {
      return next();
    }
    if (ctx.instance) {
      ctx.instance.email = ctx.instance.email.toLowerCase();
    } else {
      ctx.data.email = ctx.data.email.toLowerCase();
    }
    next();
  });
  MyUser.afterRemote('create', function(context, user, next) {
    const options = verifyEmailOptions(user);
    user.verify(options, function(err, response) {
      if (err) {
        MyUser.deleteById(user.id);
        return next(err);
      }
      context.result = {code: 'registered'};
      next();
    });
  });
};

function verifyEmailOptions(user) {
  return {
    to: user.email,
    from: senderAddress,
    html: 'Для подтверждения регистрации перейдите по ссылке <a href="http://localhost:4200/auth/confirm/{token}/{uid}">подтвердить</a>',
    subject: 'Спасибо за регистрацию',
  };
}

