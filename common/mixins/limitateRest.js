module.exports = function(Model) {
  Model.beforeRemote('addNew', function(ctx, modelInstance, next) {
    return limitate(ctx, modelInstance, next);
  });
  Model.beforeRemote('create', function(ctx, modelInstance, next) {
    return limitate(ctx, modelInstance, next);
  });

  function limitate(ctx, modelInstance, next) {
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    const Tarif = Model.app.models.Tarif;
    const UserTarif = Model.app.models.UserTarif;
    UserTarif.findOne({where: {myUserId: userId}}, async function(err, userTarif) {
      let tarif;
      if (!userTarif) {
        tarif = await Tarif.findOne({
          where: {default: true},
          include: 'limitCounts',
        });
      } else {
        tarif = await Tarif.findOne({
          where: {default: false},
          include: 'limitCounts',
        });
      }
      try {

        const cnt = await Model.count({myUserId: userId});
        if (err) {
          return next(err);
        }
        if (!tarif) {
          return next({message: 'limit'});
        }

        const limitsCounts = await tarif.limitCounts.get();
        if (!limitsCounts[Model.name] || limitsCounts[Model.name] <= cnt) {
          return next({message: 'limit'});
        }
        next();
      } catch (e) {
        return next({message: 'limit'});
      }

    });
  }
};