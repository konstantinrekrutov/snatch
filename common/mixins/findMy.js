// eslint-disable-next-line strict
module.exports = function(Model) {
  function injectUID(ctx, modelInstance, next) {
    const userId = ctx.req.accessToken && ctx.req.accessToken.userId;
    const args = {filter: {where: {myUserId: userId}}};
    if (!ctx.args) {
      ctx.args = args;
    } else if (!ctx.args.filter) {
      ctx.args.filter = args.filter;
    } else if (!ctx.args.filter.where) {
      ctx.args.filter.where = args.filter.where;
    } else if (ctx.args.filter.where) {
      ctx.args.filter.where.myUserId = args.filter.where.myUserId;
    }
    next();
  }

  Model.beforeRemote('findOne', injectUID);
  Model.beforeRemote('find', injectUID);
};
