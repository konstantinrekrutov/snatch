module.exports = function(Model) {
  Model.observe('before save', function updateTimestamp(ctx, next) {
    if (!ctx.isNewInstance) {
      return next();
    }

    let userId;

    if (ctx.instance) {
      userId = ctx.instance.myUserId;
    } else {
      userId = ctx.data.myUserId;
    }

    const Tarif = Model.app.models.Tarif;
    const UserTarif = Model.app.models.UserTarif;
    UserTarif.findOne({where: {myUserId: userId}}, async function(err, userTarif) {
      let tarif;
      if (!userTarif) {
        tarif = await Tarif.findOne({
          where: {default: true},
          include: 'limitCounts',
        });
      } else {
        tarif = await Tarif.findOne({
          where: {default: false},
          include: 'limitCounts',
        });
      }
      try {

        const cnt = await Model.count({myUserId: userId});
        if (err) {
          return next(err);
        }
        if (!tarif) {
          return next({message: 'limit'});
        }

        const limitsCounts = await tarif.limitCounts.get();
        if (!limitsCounts[Model.name] || limitsCounts[Model.name] <= cnt) {
          return next({message: 'limit'});
        }
        next();
      } catch (e) {
        return next({message: 'limit'});
      }

    });
  });
};