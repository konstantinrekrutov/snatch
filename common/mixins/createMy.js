module.exports = function (Model) {
    Model.beforeRemote( 'create', function( ctx, modelInstance, next) {
        ctx.args.data.myUserId = ctx.req.accessToken && ctx.req.accessToken.userId;
        if(!ctx.args.data.myUserId) {
            return next('noUserId');
        }
        next();
    });
}