// /* eslint-disable strict */
//
module.exports = function(app) {
  app.dataSources.mongo.connector.connect(async function(err, db) {
    let collection = db.collection('CopyTask');
    collection.createIndex({from: 1, to: 1}, {unique: true});
  });
//   const User = app.models.MyUser;
//   const Role = app.models.Role;
//   const RoleMapping = app.models.RoleMapping;
//   User.create({
//     username: 'Rick',
//     email: 'ya.rafon-92@ya.ru',
//     password: 'Sanchez',
//   }, {}, function(err, user) {
//     if (err) throw err;
//     Role.create({
//       name: 'admin',
//     }, function(err, role) {
//       if (err) throw err;
//       role.principals.create({
//         principalType: RoleMapping.USER,
//         principalId: user.id,
//       }, function(err, principal) {
//         if (err) throw err;
//         console.log('Created principal:', principal);
//       });
//     });
//   }, function(err) {
//     console.error(err);
//   });
//   const Tarif = app.models.Tarif;
//
//   Tarif.create({
//     title: 'Первый',
//     description: 'Все функции',
//     canSendRaw: true,
//     canUseProxy: true,
//     price: 4.5,
//     default: false,
//   }, function(err, tarif) {
//     tarif.limitCounts.create({
//       Creditionals: 3,
//       CopyTask: 4,
//     }, function(err, res) {
//       console.log(err, res);
//     });
//   });
//   Tarif.find({default:true, include: 'limitCounts'}, function(err, tarifs) {
//     if (!tarifs.length) {
//       createTarif();
//     }
//     tarifs.forEach((item) => {
//       item.limitCounts.destroy().then(() => {
//         item.destroy().then(() => {
//           createTarif();
//         }, () => {
//           createTarif();
//         });
//       }, () => {
//         item.destroy().then(() => {
//           createTarif();
//         }, () => {
//           createTarif();
//         });
//       });
//     });
//   });
//
//   function createTarif() {
//     Tarif.create({
//       title: 'Начало',
//       description: 'Стартовый бесплатный тариф',
//       default: true,
//     }, function(err, tarif) {
//       tarif.limitCounts.create({
//         Creditionals: 1,
//         CopyTask: 1,
//       }, function(err, res) {
//         console.log(err, res);
//       });
//     });
//   }
};
