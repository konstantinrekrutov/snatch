'use strict';

module.exports = function(app) {
  const MyUser = app.models.MyUser;
  const Helper = app.models.Helper;
  const TelegramApiAdapter = app.models.TelegramApiAdapter;



  function initTasks() {
    MyUser.find({where: {emailVerified: true}}, {include: ['copyTasks', 'userTarifs']}, function(err, users) {
      Helper.startFor(users, function(user) {
        return new Promise((resolve) => {
          user.copyTasks.find({where: {started: true}}, {include: 'creditionals'}, function(err, tasks) {
            user.userTarifs.get().then(async(userTarif) => {
              const tarifActive = userTarif && userTarif.canSendRawTo > new Date().getTime();
              if (!tarifActive && tasks.length > 1) {
                Helper.startFor(tasks, function(task) {
                  return new Promise((resolve) => {
                    task.started = false;
                    task.save().then(resolve);
                  });
                }).then(resolve);
              } else {
                const creditionalsObj = {};
                tasks.forEach(task => {
                  if (!creditionalsObj[task.creditionalsId]) {
                    creditionalsObj[task.creditionalsId] = [];
                  }
                  creditionalsObj[task.creditionalsId].push(task);
                });
                for (const i in creditionalsObj) {
                  let creditionals = await creditionalsObj[i][0].creditionals.get();
                  let client = await TelegramApiAdapter.getClient(creditionals);
                  const updManager = Helper.getManager(client, creditionals);
                  updManager.listenChatsUpdates(creditionalsObj[i]);
                }
                resolve();
              }
            });
          });
        });
      }).then(() => {
        console.log('Tasks inited');
      });
    });
  }
  setImmediate(initTasks);
};
