var server = require('./server');
var ds = server.dataSources.mongo;
var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role', 'MyUser'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
  ds.disconnect();
});

/*
*
* App api_id: 623800
* App api_hash: 9938752d7396b78569f7cebc11920027
* */