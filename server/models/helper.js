'use strict';
const fs = require('fs');
const config = require('../../config');
const nullFn = () => {
};
const UpdatesManager = require('../../custom/models/updates-manager');
const errRights = 'Задача остановлена, нет прав писать в канал';
module.exports = function(Helper) {
  Helper.UpdatesManagers = {};
  Helper.logout = function(creditionals) {
    return new Promise(async(resolve, reject) => {
      try {
        Helper.app.dataSources.mongo.connector.connect(async(err, db) => {
          let Creditionals = db.collection('Creditionals');
          const Notification = Helper.app.models.Notification;
          let CopyTask = db.collection('CopyTask');
          const resT = await CopyTask.updateMany({creditionalsId: creditionals.id}, {$set: {started: false}});
          const resC = await Creditionals.updateOne({_id: creditionals.id}, {$set: {status: 1}});
          const path = config.storagePath + '/' + creditionals.api_id + '.json';
          if (fs.existsSync(path)) {
            fs.unlinkSync(path);
          }
          await Notification.create({
            myUserId: creditionals.myUserId,
            title: 'Задачи остановлены',
            description: 'Телеграм аккаунт отключен, нужно залогинится снова и включить задачи',
            priority: 10,
          });
          resolve();
        });
      } catch (e) {
        reject(e);
      }
    });
  };

  Helper.addTask = async function(client, creditionals, task) {
    const updManager = Helper.getManager(client, creditionals);
    if (!updManager) {
      return false;
    }
    Helper.addUpdManagerTask(task, updManager);
    updManager.listenChatsUpdates();

    return true;
  };
  Helper.getPeerTo = async function(client, creditionals, task) {
    const TelegramUser = Helper.app.models.TelegramUser;
    let To = await Helper.getUserData(creditionals.telegramUserId, task.to);
    if (!To) {
      await TelegramUser.renewContacts(client, creditionals, creditionals.telegramUserId);
      To = await Helper.getUserData(creditionals.telegramUserId, task.to);
    }
    if (!To) {
      const message = 'cant get To';
      throw new Error(message);
    }
    const telegramUserTo = await TelegramUser.findById(task.to);
    return await Helper.makePeer(telegramUserTo, To['access_hash']);
  };

  Helper.startTask = function(task, noChangeStarted) {
    const TelegramApiAdapter = Helper.app.models.TelegramApiAdapter;
    return new Promise((resolve, reject) => {
      task.creditionals.get().then(creditionals => {
        if (!creditionals) {
          return reject();
        }
        TelegramApiAdapter.getClient(creditionals).then(client => {
          const updManager = Helper.getManager(client, creditionals);
          if (!updManager) {
            return reject();
          }

          Helper.addUpdManagerTask(task, updManager);
          if (updManager.chatIds.length === 1) {
            updManager.startChatsUpdates();
          }
          if (!noChangeStarted) {
            task.started = true;
            task.save().then(resolve, reject);
          } else {
            resolve();
          }

        }, reject);

      }, reject);
    });
  };

  Helper.addUpdManagerTask = function(task, updManager) {
    const index = updManager.chatIds.indexOf(task.from);
    if (index === -1) {
      updManager.chatIds.push(task.from);
    }
    if(!updManager.tasks[task.from]) {
      updManager.tasks[task.from] = {[task.to]:task};
    } else {
      updManager.tasks[task.from][task.to] = task;
    }
  };

  Helper.deleteUpdManagerTask = function(task, updManager) {
    const index = updManager.chatIds.indexOf(task.from);
    if (index !== -1) {
      updManager.chatIds.splice(index, 1);
    }
    updManager.tasks[task.from] &&  updManager.tasks[task.from][task.to] && delete updManager.tasks[task.from][task.to];
  };

  Helper.updateTask = function(oldTask, updatedTask) {
    const TelegramApiAdapter = Helper.app.models.TelegramApiAdapter;
    return new Promise((resolve, reject) => {
      oldTask.creditionals.get().then(creditionals => {
        if (!creditionals) {
          return reject();
        }
        TelegramApiAdapter.getClient(creditionals).then(client => {
          const updManager = Helper.getManager(client, creditionals);
          if (!updManager) {
            return reject();
          }
          Helper.deleteUpdManagerTask(oldTask, updManager);
          Helper.addUpdManagerTask(updatedTask, updManager);
          updManager.listenChatsUpdates();
          resolve();
        }, reject);
      });
    });
  };
  Helper.stopTask = function(task, noChangeStarted) {
    return new Promise((resolve, reject) => {
      task.creditionals.get().then(creditionals => {
        if (!creditionals) {
          return reject();
        }
        const updManager = Helper.UpdatesManagers[creditionals.telegramUserId];
        if (!updManager) {
          return resolve();
        }
        Helper.deleteUpdManagerTask(task, updManager);
        if (updManager.chatIds.length === 0) {
          updManager.stopChatsUpdates();
        }
        if (!noChangeStarted) {
          task.started = false;
          task.save().then(resolve, reject);
        } else {
          resolve();
        }
      }, reject);
    });
  };

  Helper.stopUpdManager = function(updManager) {
    updManager.stopChatsUpdates();
    delete Helper.UpdatesManagers[updManager.telegramUserId];
  };
  Helper.makePeer = async function(telegramUser, access_hash) {
    switch (telegramUser['_']) {
      case 'user': {
        return {
          '_': 'inputPeerUser',
          'user_id': telegramUser.id,
          'access_hash': access_hash,
        };
      }
      case 'channel': {
        return {
          '_': 'inputPeerChannel',
          'channel_id': telegramUser.id,
          'access_hash': access_hash,
        };
      }
      case 'chat': {
        return {
          '_': 'inputPeerChat',
          'chat_id': telegramUser.id,
        };
      }
      default : {
        throw new Error(telegramUser['_'] + ' is non-known');
      }
    }
  };
  Helper.getPeers = async function(client, creditionals, task) {
    const TelegramUser = Helper.app.models.TelegramUser;

    let From = await Helper.getUserData(creditionals.telegramUserId, task.from);
    let To = await Helper.getUserData(creditionals.telegramUserId, task.to);
    if (!To || !From) {
      await TelegramUser.renewContacts(client, creditionals, creditionals.telegramUserId);
      From = await Helper.getUserData(creditionals.telegramUserId, task.from);
      To = await Helper.getUserData(creditionals.telegramUserId, task.to);
    }
    if (!To || !From) {
      const message = 'cant get ' + (To ? 'From' : 'To');
      throw new Error(message);
    }

    const telegramUserFrom = await TelegramUser.findById(task.from);
    const telegramUserTo = await TelegramUser.findById(task.to);
    const peerFrom = await Helper.makePeer(telegramUserFrom, From['access_hash']);
    const peerTo = await Helper.makePeer(telegramUserTo, To['access_hash']);
    return {peerFrom, peerTo};
  };
  Helper.getManager = function(client, creditionals) {
    const Sender = Helper.app.models.Sender;
    const UserTarif = Helper.app.models.UserTarif;
    const Creditionals = Helper.app.models.Creditionals;
    const TelegramApiAdapter = Helper.app.models.TelegramApiAdapter;
    if (!Helper.UpdatesManagers[creditionals.telegramUserId]) {
      Helper.UpdatesManagers[creditionals.telegramUserId] = new UpdatesManager(client, creditionals, Helper);
      Helper.UpdatesManagers[creditionals.telegramUserId].on('error', (err) => {
        console.error(err.err);
        Helper.stopUpdManager(Helper.UpdatesManagers[creditionals.telegramUserId]);
      });
      Helper.UpdatesManagers[creditionals.telegramUserId].on('newMessages', function(messages) {
        const fromId = messages[0].from_id;
        const toId = messages[0].to_id.channel_id || messages[0].to_id.chat_id || messages[0].to_id.user_id;
        const tasks = this.tasks[fromId] || this.tasks[toId];

        if (tasks) {
          Object.values(tasks).forEach(task => {
            Helper.getPeers(this.client, this.creditionals, task).then(peers => {
              UserTarif.findOne({myUserId: task.myUserId}).then((userTarif) => {
                let f;
                const sendRaw = (task.sendRaw || task.creditionalsProxyId) && userTarif && userTarif.canSendRawTo.getTime() > new Date().getTime();
                if (sendRaw) {
                  f = Sender.copyAsRaw;
                } else {
                  f = Sender.forwardMessages;
                }

                const errFn = (error) => {
                  if (error.type === 'CHAT_ADMIN_REQUIRED') { // Notify User
                    task.started = false;
                    task.save().then(nullFn, console.error);
                  }
                };
                if (task.creditionalsProxyId && userTarif && userTarif.canUseProxyTo.getTime() > new Date().getTime()) {
                  Creditionals.findOne({
                    where: {
                      myUserId: task.myUserId,
                      id: task.creditionalsProxyId,
                    },
                  }).then(creditionals => {
                    if (!creditionals || creditionals.status !== 3) {
                      console.error('not_auth');
                      return;
                    }
                    TelegramApiAdapter.getClient(creditionals).then(client => {
                      if (!client) {
                        console.error('no_client');
                        return;
                      }
                      Helper.getPeerTo(client, creditionals, task).then((peerTo) => {
                        if (!peerTo) {
                          console.error('no_peerTo');
                          return;
                        }
                        peers.peerTo = peerTo;
                        f(client, peers, messages).then(nullFn, errFn);
                      }, (err) => {
                        console.error(err);
                        Helper.stopTask(task).then(() => {
                          task.started = false;
                          task.save();
                        }, err => {
                          console.error(err);
                        });
                      });
                    }, console.error);
                  }, console.error);
                } else {
                  f(client, peers, messages).then(nullFn, errFn);
                }
              });
            }, err => {
              console.error(err);
              Helper.stopTaskWithNotify(task);
            });
          });



        }
      });
    }

    return Helper.UpdatesManagers[creditionals.telegramUserId];
  };
  Helper.stopTaskWithNotify = function(task) {
    Helper.stopTask(task).then(() => {
      Helper.app.models.Notification.createNotification(errRights);
    });
  };
  Helper.getUserData = function(telegramUserId, id) {
    return Helper.app.models.TelegramUserContacts.findOne({
      where: {
        telegramUserId: telegramUserId,
        contactId: id,
      },
    });
  };
  Helper.pause = function(sec) {
    return new Promise(resolve => {
      setTimeout(function() {
        resolve();
      }, sec * 1000);
    });
  };

  Helper.startFor = function(obj, callback, i) {
    return new Promise((resolve) => {
      if (!obj) {
        return resolve();
      }
      i = i ? i : 0;
      if (i === obj.length) {
        return setImmediate(resolve);
      }
      callback.call(this, obj[i]).then(() => {
        Helper.startFor(obj, callback, ++i).then(() => {
          resolve();
        });
      });
    });
  };
};
