'use strict';
const {MTProto} = require('telegram-mtproto');
const {Storage} = require('mtproto-storage-fs');
const fs = require('fs');
const config = require('../../config');

const clients = {};

module.exports = function (TelegramApiAdapter) {
    TelegramApiAdapter.getClient = async function (creditionals) {
        const Creditionals = TelegramApiAdapter.app.models.Creditionals;
        let cid;
        if (typeof creditionals === 'object') {
            cid = creditionals.id;
        } else {
            cid = creditionals;
            creditionals = await Creditionals.findById(cid);
        }
        if (clients[cid]) {
            return clients[cid];
        }
        if (!creditionals) {
            return false;
        }

        const api = {
            invokeWithLayer: 0xda9b0d0d,
            layer: 57,
            initConnection: 0x69796de9,
            app_version: '1.0.1',
            lang_code: 'ru',
            api_id: creditionals.api_id
        };
        const server = {dev: config.dev, webogram: true};
        const path = config.storagePath + '/' + creditionals.api_id + '.json';
        const storage = new Storage(path);
        const options = {server, api, app: {storage}};
        const telegram = MTProto(options);
        clients[cid] = (
            function (telegram, creditionals) {
                return function (method, options, additional, ms = 20000) {
                    return new Promise((resolve, reject) => {
                        let tmout = setTimeout(function() {
                            reject('Promise timed out after ' + ms + ' ms');
                        }, ms);
                        try {
                            telegram(method, options, additional).then(result => {
                                clearTimeout(tmout);
                                resolve(result);
                            }, error => {
                                clearTimeout(tmout);
                                if (error.type === 'AUTH_KEY_UNREGISTERED') {
                                    const Helper = TelegramApiAdapter.app.models.Helper;
                                    Helper.logout(creditionals).then(() => {
                                        delete clients[cid];
                                        return reject(error);
                                    }, (e) => {
                                        return reject(e);
                                    });
                                } else if(error.type === 'API_ID_INVALID') {
                                    TelegramApiAdapter.deleteClient(creditionals).then(() => {
                                        return reject(error);
                                    }, (err) => {
                                        console.error(err);
                                        return reject(error);
                                    });
                                } else {
                                    return reject(error)
                                }
                            }).catch(exception => {
                                clearTimeout(tmout);
                                console.error(exception);
                                reject(exception);
                            });
                        } catch (e) {
                            clearTimeout(tmout);
                            reject(e);
                        }
                    })
                }
            })(telegram, creditionals);
        return clients[cid];
    };
    TelegramApiAdapter.deleteClient = async function (creditionals) {
        const Creditionals = TelegramApiAdapter.app.models.Creditionals;
        const Helper = TelegramApiAdapter.app.models.Helper;
        const updManager = Helper.UpdatesManagers[creditionals.telegramUserId];
        if(updManager) {
            Helper.stopUpdManager(updManager);
        }
        if (!creditionals) {
            return;
        }
        try {
            clients[creditionals.id] && await clients[creditionals.id]('auth.logOut');
        } catch (e) {
            console.error(e);
        }
        clients[creditionals.id] && delete clients[creditionals.id];
        const path = config.storagePath + '/' + creditionals.api_id + '.json';
        if (fs.existsSync(path)) {
            fs.unlinkSync(path);
        }
        await Creditionals.destroyById(creditionals.id);
    };
};
