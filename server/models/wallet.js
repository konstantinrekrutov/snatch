'use strict';
const {seedUtils, broadcast, transfer} = require('@waves/waves-transactions');
const {SeedAdapter, Adapter} = require('@waves/signature-adapter');
Adapter.initOptions({networkCode: 'T'.charCodeAt(0)});
const config = require('../../config');
const request = require('request');
const nodeUrl = 'https://testnodes.wavesnodes.com/';

module.exports = function(Wallet) {
  Wallet.createWallet = function(tarif, myUserId) {
    return new Promise((resolve, reject) => {
      const seed = seedUtils.generateNewSeed();
      const encrypted = seedUtils.encryptSeed(seed, config.rs);
      tarif.wallets.create({
        myUserId: myUserId,
        tokenId: encrypted,
      }, (err, wallet) => {
        if (err) {
          reject(err);
        } else {
          resolve(wallet);
        }
      });
    });
  };

  Wallet.sendPayToUser = function(wallet, tarif) {
    return new Promise((resolve, reject) => {
      const restoredTokenId = seedUtils.decryptSeed(wallet.tokenId,
        config.rs);
      const senderAdapter = new SeedAdapter(restoredTokenId);
      senderAdapter.getSeed().then(payerSeed => {
        const signedTranserTx = transfer({
          amount: ((tarif.price * 100000000) - 100000),
          recipient: config.myWallet,
          assetId: 'WAVES',
          feeAssetId: 'WAVES',
          fee: 100000,
          timestamp: Date.now(),
        }, payerSeed);
        broadcast(signedTranserTx, nodeUrl).then(resolve, reject);
      });
    });
  };

  Wallet.decrypt = function(wallet) {
    return new Promise((resolve, reject) => {
      try {
        const restoredTokenId = seedUtils.decryptSeed(wallet.tokenId,
          config.rs);
        const adapter = new SeedAdapter(restoredTokenId);
        resolve(adapter);
      } catch (e) {
        reject(e);
      }
    });
  };
  Wallet.getOrCreateWallet = function(tarif, myUserId) {
    return new Promise((resolve, reject) => {
      Wallet.getWallet(tarif, myUserId).then(wallet => {
        Wallet.decrypt(wallet).then(adapter => {
          adapter.getAddress().then(resolve, reject);
        });
      }, error => {
        console.error(error);
        Wallet.createWallet(tarif, myUserId).then(wallet => {
          Wallet.decrypt(wallet).then(adapter => {
            adapter.getAddress().then(resolve, reject);
          });
        }, reject);
      });
    });
  };
  Wallet.getWallet = function(tarif, myUserId) {
    return new Promise((resolve, reject) => {
      tarif.wallets.findOne({myUserId}, (err, wallet) => {
        if (err || !wallet) {
          reject(err);
        } else {
          resolve(wallet);
        }
      });
    });
  };

  Wallet.getNewTransactions = function(address, dateFrom = 0, result = []) {
    return new Promise((resolve, reject) => {
      const url = nodeUrl + 'transactions/address/' + address + '/limit/50';
      request.get({url, json: true}, (err, response, body) => {
        if (err) {
          return reject(err);
        }
        for (const i in body) {
          const transactions = body[i];
          if (!transactions.length) {
            return resolve(result);
          }
          for (const j in transactions) {
            const transaction = transactions[j];
            if (transaction && transaction.timestamp > dateFrom) {
              result.push(transaction);
            } else {
              return resolve(result);
            }
          }
        }
        resolve(result);
      });
    });
  };


  Wallet.checkPay = function(wallet, tarif) {
    return new Promise((resolve, reject) => {
      const needPrice = tarif.price * 100000000;
      let payedPrice = 0;
      const restoredTokenId = seedUtils.decryptSeed(wallet.tokenId,
        config.rs);
      const recipientAdapter = new SeedAdapter(restoredTokenId);
      recipientAdapter.getAddress().then(address => {
        Wallet.getNewTransactions(address, wallet.lastBuyTransaction)
          .then(transactions => {
            transactions.forEach(transaction => {
              if (transaction.recipient === address) {
                payedPrice += transaction.amount;
              }
            });
            if (payedPrice >= needPrice) {
              wallet.lastBuyTransaction = transactions[0].timestamp;
              wallet.save(err => {
                if (err) {
                  console.error(err);
                  return reject(err);
                }
                resolve();
              });
            } else {
              reject(payedPrice);
            }
          }, err => {
            console.error(err);
            reject(err);
          });
      });
    });
  };
};
