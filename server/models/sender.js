'use strict';

module.exports = function(Sender) {
  Sender.forwardMessages = function(client, peers, messages) {
    return new Promise((resolve, reject) => {
      client('messages.forwardMessages', {
        to_peer: peers.peerTo,
        from_peer: peers.peerFrom,
        id: messages.map(message => {
          return message.id;
        }),
        random_id: messages.map(message => {
          return Math.floor(message.id * 0xFFFFFFFF).toString();
        }),
      }).then(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  };

  Sender.copyAsRaw = function(client, peers, messages) {
    return new Promise(async(resolve, reject) => {
      for (const i in messages) {
        const message = messages[i];
        const options = {};
        let method;
        if (message.reply_markup) {
          options['reply_markup'] = message.reply_markup;
        }
        if (message.reply_to_msg_id) {
          options['reply_to_msg_id'] = message.reply_to_msg_id;
        }
        if (message.entities) {
          options['entities'] = message.entities;
        }
        options['peer'] = peers.peerTo;
        options['random_id'] = Math.floor(Math.random().toString(10).substr(2, 9) * 0xFFFFFFFF).toString();
        if (message.message) {
          options['message'] = message.message;
          method = 'messages.sendMessage';
        } else {
          options['media'] = makeInputMedia(message.media);
          if(!options['media']) {
            continue;
          }
          method = 'messages.sendMedia';
        }
        try {
          await client(method, options);
        } catch (e) {
          if (e.type === 'CHAT_ADMIN_REQUIRED') {
            return reject(e);
          }
        }
      }
      resolve();
    });

  };

  function makeInputMedia(media) {
    switch (media._) {
      case 'messageMediaPhoto': {
        return {'_':'inputMediaPhoto', id: {'_':'inputPhoto', id:media.photo.id, access_hash:media.photo.access_hash}, caption: media.caption};
      }
      case 'messageMediaDocument': {
        return {'_':'inputMediaDocument', id: {'_':'inputDocument', id:media.document.id, access_hash:media.document.access_hash}, caption: media.caption};
      }
      case 'messageMediaGeo': {
        return false;//{'_':'inputMediaGeoPoint', geo_point: {'_':'inputGeoPoint', lat:media.geo.lat, long:media.geo.long}};
      }
      case 'messageMediaUnsupported': {
        return false;
      }
      default : {
        return media;
      }
    }
  }
};
